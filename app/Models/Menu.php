<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends \TCG\Voyager\Models\Menu
{
    use HasFactory;

    protected $hidden = ['created_at', 'updated_at'];

}
