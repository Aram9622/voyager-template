<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Comment;
use TCG\Voyager\Models\Category;

class Post extends \TCG\Voyager\Models\Post
{
    use HasFactory;

    protected $fillable = ['title', 'category_id', 'seo_title', 'excerpt', 'body', 'image', 'slug', 'meta_description', 'meta_keywords', 'status','featured', 'image_position'];
    protected $attributes = ['image'];

    public function getImageAttribute(){
        return asset('storage/'.$this->attributes['image']);
    }


    public function comment(){
        return $this->hasMany(Comment::class);
    }

    public function post_comment(){
        return $this->belongsToMany(Comment::class, 'post_comments');
    }

    public function getTwoLastPost($category, $limit)
    {
        return self::query()->where('category_id', $category)->orderByDesc('id')->limit($limit)->get();
    }

    public function post_tag(){
        return $this->belongsToMany(Tag::class, 'post_tags');
    }

    public function getNameOfClass()
    {
        return static::class;
    }

}
