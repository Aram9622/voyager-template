<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'comments',
        'name',
        'email',
        'website',
        'post_id',
        'status'
    ];

    public function post_comment(){
        return $this->belongsToMany(Post::class, 'post_comments');
    }
}
