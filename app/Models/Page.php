<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Page extends \TCG\Voyager\Models\Page
{
    use HasFactory, HasSlug;

    protected $fillable = ['title', 'slug', 'template', 'template_fields', 'template_images', 'template_id', 'seo'];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public static function store($data, $id = null){
        if($id){
            self::query()->where('id', $id)->update($data);
        }
        else {
            self::query()->create($data);
        }
    }

    public static function cascadeDeleteWithMenuItem($slug){
        $menuItem = MenuItem::query()->where('url', $slug);
        if($menuItem->first()){
            $menuItem->delete();
        }
        return true;
    }

    public function getNameOfClass()
    {
        return static::class;
    }

    public function templateType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Template::class, 'template_id')->select(['id','type']);
    }

    public static function checkUniqSlug($slug): bool
    {
        return self::query()->where('slug', $slug)->first() ? true : false;
    }
}
