<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuItem extends \TCG\Voyager\Models\MenuItem
{
    use HasFactory;

    public static function store($data, $id = null){
        if($id){
            self::query()->where('id', $id)->update($data);
        }
        else{
            self::query()->create($data);
        }
    }
}
