<?php

namespace App\Models;

use App\library\CompareImages;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;


class Media extends Model
{
    protected $table = 'medias';
//    protected $attributes = [
//        'title',
//        'alt',
//        'caption',
//        'description'
//    ];
    protected $fillable = [
        'file',
        'alt',
        'title',
        'caption',
        'description',
        'type',
        'size',
    ];

    public static function getImageInMediaByPath($path, $full = false){
        $fullPath = $full ? $path : asset('storage'.$path);
        return self::query()->where('file', $fullPath)->first();
    }

    public function getValueNotEmpty($value, $key){
        return $value['identify'] && !empty($value['result'][$key]) ? $value['result'][$key] : '';
    }

    public static function compareTwoImage($file){
        $medias = self::query()->get();
        $compare = new CompareImages();
        $identify = false;
        $result = [];
        if($medias->count() > 0){
            foreach ($medias->toArray() as $item){
//                dd($file, $item['file']);
                if($compare->compare(explode('storage', $file)[1], explode('storage', $item['file'])[1]) == '0'){

                    $result['alt'] = $item['alt'];
                    $result['title'] = $item['title'];
                    $result['description'] = $item['description'];
                    $result['caption'] = $item['caption'];
                    $result['file'] = $item['file'];
                    $identify = true;
                }
            }
        }


        return ['identify' => $identify, 'result' => $result];
    }


    public function getTitleAttribute(){
        return $this->attributes['title'] == null ? 'N/A' : $this->attributes['title'];
    }

    public function getAltAttribute(){
        return $this->attributes['alt'] == null ? 'N/A' : $this->attributes['alt'];
    }

    public function getCaptionAttribute(){
        return $this->attributes['caption'] == null ? 'N/A' : $this->attributes['caption'];
    }

    public function getDescriptionAttribute(){
        return $this->attributes['description'] == null ? 'N/A' : $this->attributes['description'];
    }

}
