<?php

namespace App\Helper;

use App\Helper\Template as TemplateHelper;

class Template {
    const TEXT = 'text';
    const LINK = 'link';
    const IMAGE = 'image';
    const TEXTAREA = 'textarea';
    const BUTTON = "button";
    const BG_IMAGE = "bgImage";
    const HIDDEN = 'hidden';
    const ALL_TYPES_FORM = [self::TEXT, self::TEXTAREA, self::IMAGE, self::BG_IMAGE, self::HIDDEN, self::LINK, self::BUTTON, 'texts', 'list','texts1'];

    public static function setFieldName($name, $field): string
    {
        return $name . '_' . $field;
    }

    public static function inputTag($name, $type, $placeholder = null, $value = null, $id = null): string
    {
        $expTitle = explode('_', $name)[1] == 'title' ? 'title' : $id;

        $btn = $type == "file" ? "btn btn-info template-image-file" : "";
        return "<input class='form-control $btn' type='$type' name='$name' value='$value' placeholder='$placeholder' id='$expTitle'>";
    }

    public static function textareaTag($name, $placeholder, $value = null, $id = null): string
    {
        return "<textarea class='form-control' name='$name' id='$name' cols='30' rows='10' placeholder='$placeholder'>$value</textarea>";
    }

    public static function createTag($tag, $tagName, $placeholder = null, $tagValue = null, $id = null): string
    {
        $output = "<div class='form-group'>";
        if ($tag === self::TEXT) {
            $output .= self::inputTag($tagName, $tag, $placeholder, $tagValue, $id);
        } else if ($tag === self::TEXTAREA) {
            $output .= self::textareaTag($tagName, $placeholder, $tagValue);
        } else if ($tag === self::IMAGE) {
            $output .= self::inputTag($tagName, 'file', $placeholder, $tagValue, $id);
        } else if ($tag === self::HIDDEN) {
            $output .= self::inputTag($tagName, $tag, null, $tagValue);
        }
        $output .= '<div class="errorMessage"></div>';
        $output .= "</div>";
        return $output;
    }

    public static function checkArrayKeyExists($array, $key)
    {
        return array_key_exists($key, $array) ? $array[$key] : null;
    }

    public static function ValidationTemplate($type, $text){
        if($type == TemplateHelper::TEXT){
            if(strlen($text) < 1 || strlen($text) > 255){
                return  "Text length mast be between in 2, 255";
            }
        }
        return true;
    }
}
