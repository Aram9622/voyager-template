<?php

namespace App\Helper;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Helper
{
    const SERVER_ROOT = "/home/austinmerchantse/public_html/example/washington/storage/app/public";
    const LOCAL_ROOT = "C:\Users\Admin\Desktop\back\storage\app\public";
    const PORT = '192.168.5.153';
    const PORT_LOCAL = '127.0.0.1';
    const UPLOAD_DIR = 'uploads';
    const UPLOAD_TEMPLATE_DIR = 'uploads/template';
    public function responseHandler($message, $status, $action, $data)
    {
        return response()->json(["message" => $message, "action" => $action, "data" => $data], $status);
    }

    public static function responseHandlerStatic($message, $status, $action, $data)
    {
        return response()->json(["message" => $message, "action" => $action, "data" => $data], $status);
    }

    public static function apiAuth(){
        return auth()->guard('api');
    }

    public static function curlRequest($url){
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $headers = array(
            "Accept: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }

    public static function PathRootWithPrototype(){
//        dd($_SERVER['SERVER_NAME']);
        if($_SERVER['SERVER_NAME'] == self::PORT || $_SERVER['SERVER_NAME'] == self::PORT_LOCAL){
            return self::LOCAL_ROOT;
        }
        else {
            return self::SERVER_ROOT;
        }
        return false;
    }

    public static function processUpload($image, $path): string
    {
            $file = $image;
            $fileName = Str::random(32) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path($path), $fileName);
            return asset($path . '/' . $fileName);
    }

    public static function uploadFileMultiOrSingle($data){
        $keys = array_keys($data);
        $images = [];
        try {
            foreach ($keys as $item){
                foreach ($data[$item] as $key => $value) {
//                 /* @var UploadedFile $file */
                    if(!is_string($data[$item][$key]) && !is_array($data[$item][$key])){
                        $file = $data[$item][$key];
                        if($file !== null){
                            $getClientOriginalName = $file->getClientOriginalName();
                            $fileName = Str::random(32). '.'.$file->getClientOriginalExtension();
                            $file->move(public_path('uploads'),$fileName);
                            $data[$item][$key] = asset('uploads/'.$fileName);
                            $images[$key] = asset('uploads/'.$fileName);
                        }
                    }
                    else {
                        if(is_array($data[$item][$key])){
                            foreach ($data[$item][$key] as $keyImage => $valueImage) {
                                $file = $data[$item][$key][$keyImage];
                                foreach (array_keys($file) as $img) {

                                    if(!is_string($file[$img]) && !is_array($file[$img]) && isset($file[$img])){
                                        $file = $data[$item][$key][$keyImage][$img];
                                        $getClientOriginalName = $file->getClientOriginalName();
                                        $fileName = Str::random(32). '.'.$file->getClientOriginalExtension();
                                        $file->move(public_path('uploads'),$fileName);
                                        $data[$item][$key][$keyImage][$img] = asset('uploads/'.$fileName);
                                        $images[$key][$keyImage][$img] = asset('uploads/'.$fileName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return ['data' => $data, 'images' => $images];
        }catch (\Exception $e){
            return ["error" => $e->getMessage()];
        }

    }

    public static function uploadFileForOtherTemplate($data){
        try {
            foreach ($data as $key => $item) {
                $keys = array_keys($item);
                foreach ($keys as $typeKey){
                    $typeKeys = array_keys($data[$key][$typeKey]);
                    if(count($typeKeys) == 1){
                        if(!is_string($data[$key][$typeKey][$typeKeys[0]]) && !is_null($data[$key][$typeKey][$typeKeys[0]])){
                            if(!is_array($data[$key][$typeKey][$typeKeys[0]])){
                                $data[$key][$typeKey][$typeKeys[0]] = self::processUpload($data[$key][$typeKey][$typeKeys[0]], self::UPLOAD_DIR);
                            }
                            else {
                                $data[$key][$typeKey][$typeKeys[0]] = array_reverse($data[$key][$typeKey][$typeKeys[0]],true);
                                foreach($data[$key][$typeKey][$typeKeys[0]] as $itemImageKey => $itemImageValue){
                                    $MultiImageKeys = array_keys($itemImageValue)[0];
                                    $data[$key][$typeKey][$typeKeys[0]][$itemImageKey][$MultiImageKeys] = self::processUpload($data[$key][$typeKey][$typeKeys[0]][$itemImageKey][$MultiImageKeys], self::UPLOAD_DIR);
                                }
                            }

                        }
                    }
                }
            }
        }catch (\Exception $e) {
            dd($e);
        }


        return $data;
    }

    public static function uploadOneImage($req, $path){
        if($req->hasFile('file')){
//            $file = $req->file;
//            $fileDetails = ['fileName' => Str::random(32). '.'.$file->getClientOriginalExtension()];
//            $file->move(public_path($path),$fileDetails['fileName']);
//            $fileDetails['fileLink'] = asset($path.'/'.$fileDetails['fileName']);

            $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs($path, $fileName, 'public');
            return ['fileName' => $fileName, 'fileLink' => '/storage/' . $filePath];
        }
       return false;
    }

}
