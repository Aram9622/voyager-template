<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use Illuminate\Http\Request;

class SendRequestToPageSpeed extends Controller
{
    private $apiKey = 'AIzaSyBcKuNiWrUfyGoRoKfs2IR6M66gIExGEps';
    private $strategy = 'DESKTOP';
    private $category = 'SEO';
    private $urlCheck = 'https://newwadc.washingtondcautotransport.com/career-page-new';
    private $rootApi = 'https://pagespeedonline.googleapis.com/pagespeedonline/v5/runPagespeed';
    public function send(){
        $url = "$this->rootApi?url=$this->urlCheck&category=$this->category&strategy=$this->strategy&key=$this->apiKey";
        $resp = json_decode(Helper::curlRequest($url));
        return response()->json($resp);
    }
}
