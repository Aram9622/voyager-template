<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Repositories\MailRepository;
use Illuminate\Http\Request;

class MailController extends Controller
{
    protected $repository;

    public function __construct(MailRepository $repository)
    {
        $this->repository = $repository;
    }


    public function sendEmail(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $this->repository->sendEmail($request->all(),'info-mail');
            return Helper::responseHandlerStatic("Successfully send", 200, true, []);
        }catch (\Exception $exception){
            return Helper::responseHandlerStatic("Error Message", 500, false, ["mailError" => $exception->getMessage()]);
        }

//        return response()->json([
//            'success' => true,
//            'message' => 'Send Mail has ben successfully',
//            'data' => $this->repository->sendEmail($request->all()),
//        ]);
    }
}
