<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service\BlogService;
use App\Helper\Helper;
use App\Http\Requests\BlogCommentsRequest;

class BlogController extends Controller
{
    public $blog;
    public function __construct(BlogService $blog){
        $this->blog = $blog;
    }

    public function getBlogs($slug = null){
        $blog = $slug ? $this->blog->getById($slug) : $this->blog->get();
        return Helper::responseHandlerStatic("get blog message", 200, true, $blog);
    }

    public function getBlogsByCategory(Request $request){
        $limit = 10;
        $page = $request->page;
        $slug = $request->slug;
        $pagesCount = count($this->blog->getByCategory($slug, true));
        return Helper::responseHandlerStatic("get blog message", 200, true, [
            'pageCount' => round($pagesCount / $limit),
            'currentPage' => $page,
            'posts' => $this->blog->getByCategory($slug, false, $limit, $page),
        ]);
    }

    public function createCommentsBlog(BlogCommentsRequest $request){
        return Helper::responseHandlerStatic($this->blog->createComments($request), 200, true, []);
    }

    public function getTwoLastData(Request $request): \Illuminate\Http\JsonResponse
    {
        return Helper::responseHandlerStatic('', 200, true, $this->blog->getLastDataWithArguments($request->all()));
    }
}
