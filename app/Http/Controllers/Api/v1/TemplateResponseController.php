<?php

namespace App\Http\Controllers\Api\v1;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Models\Menu;
use Illuminate\Support\Facades\Log;
use TCG\Voyager\Facades\Voyager;

class TemplateResponseController extends Controller
{
    public function menuItems(): \Illuminate\Http\JsonResponse
    {
        $menu = Menu::query()->with(['items' => function($query) {
            $query->select(['id', 'menu_id', 'title', 'url','order','parent_id']);
        }])->where('id', 2)->first();

        $menu->logo = Voyager::image(setting('site.logo'));
        $menu->logo_second = Voyager::image(setting('site.second_logo'));
        $menu->social = Menu::query()->with(['items' => function($query) {
            $query->select(['id', 'menu_id', 'title', 'url','order','parent_id']);
        }])->where('id', 3)->first();
        return Helper::responseHandlerStatic('Menus List', 200, true, $menu);
    }

    public function getPageBySlug($slug): \Illuminate\Http\JsonResponse
    {
        if(!Page::query()->where('slug', $slug)->first()){
            return Helper::responseHandlerStatic("Not found", 404, false, []);
        }
        $page = Page::query()->with(['templateType'])->where('slug', $slug)->select(['id', 'title', 'slug', 'template', 'template_id', 'is_main_page', 'template_images'])->first();
        $page->template = json_decode($page->template, true);
        $newArray = [];
        if($page->is_main_page || $page->templateType->type !== 'page'){
            $refactorTemplate = array_keys($page->template);
            foreach ($refactorTemplate as $key) {
                $expItem = explode('_', $key);
                if(!is_numeric(substr($key, -1))){
                    $newArray[$expItem[0]][$expItem[1]] = $page->template[$key];
                }
                else if(count($expItem) == 2 && is_numeric(substr($key, -1))) {

                    $newArray[$expItem[0]]['boxes'][$expItem[1]] = $page->template[$key];
                }
                else {

                    if(substr($key, -1) == $expItem[2]){
                        if(isset($newArray[$expItem[0]]) && !is_numeric(array_keys($newArray[$expItem[0]])[0])){
                            $newArray[$expItem[0]][$expItem[1]][$expItem[2]] = $page->template[$key];
                        }
                        else {
                            $newArray[$expItem[0]][$expItem[2]][$expItem[1]] = $page->template[$key];
                        }
                    }

                }
            }
            $page->template = $newArray;

        }
        else {
            $page->template_id = 'template'.$page->template_id;
            $templateImages = json_decode($page->template_images, true);
            $templateSection = array_keys($page->template)[0];
            $template = array_merge($page->template[$templateSection], $templateImages);
            $template = [
                'id' => $page->id,
                'slug' => $page->slug,
                'template' => [$templateSection => $template],
                'template_id' => $page->template_id
            ];
            $page = $template;
        }

        return Helper::responseHandlerStatic("Page Information", 200, true, $page);
    }

    public function getPagesWithTemplateType($type){
        $page = Page::query()
            ->with('templateType')
            ->whereHas('templateType', function ($query) use ($type){
                $query->where('type', $type);
            })
            ->get(['id', 'title', 'slug', 'template', 'template_id'])
            ->map(function ($item){
                $template = json_decode($item->template, true);
                $templateRefactorForShow = [
                    "shortDescription" => $template['service_shortDescription'],
                    "image" => $template['service_image']
                ];
                $item->template = $templateRefactorForShow;
                return $item;
            });

        return Helper::responseHandlerStatic('', 200, true, $page);
    }
}
