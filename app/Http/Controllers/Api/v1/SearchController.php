<?php

namespace App\Http\Controllers\Api\v1;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Service\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $service;
    public function __construct(SearchService $service)
    {
        $this->service = $service;
    }

    public function search(Request $request): \Illuminate\Http\JsonResponse
    {
        return Helper::responseHandlerStatic('', 200, true, $this->service->dynamicQuery($request));
    }

    public function searchByTag($tag): \Illuminate\Http\JsonResponse
    {
        return Helper::responseHandlerStatic('', 200, true, $this->service->getByTag($tag));
    }
}
