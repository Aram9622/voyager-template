<?php

namespace App\Http\Controllers\Api\v1\Form;

use App\Contracts\QuoteFormInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuoteFormController extends Controller
{
    public $form;
    public function __construct(QuoteFormInterface $form)
    {
        $this->form = $form;
    }

    public function sendEmailWithQuoteData(Request $request){
        $this->form->setDataToMailForm($request->all());
    }
}
