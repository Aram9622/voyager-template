<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Repositories\MailRepository;
use Illuminate\Http\Request;

class ActionStatusController extends Controller
{
    public function changeActionStatus($id, Request $request){
        $table = $request->table;
        if($table == 'post' || $table == 'posts' || $table == 'Post' || $table == 'POST'){
            $table = Post::query();
        }
        else if($table == 'comment' || $table == 'comments' || $table == 'Comment' || $table == 'COMMENT' ){
            $table = Comment::query();
        }
        $table->where('id', $id)->update([$request->field => $request->status]);
        return response()->json("Success");
    }

    public function checkExistsSEO(Request $request){
        $modelName = $request->model;
        $model = "App\\Models\\$modelName";
        $route = route('send');
        return response()->json(['data' => $model::query()->where('seo', 1)->get(['id', 'seo']), 'route' => $route]);
    }

    public function changeSEOStatus(Request  $request){
        $modelName = ucfirst(substr($request->model, 0 ,-1));
        $ids = explode(',', $request->ids);
        $model = "App\\Models\\$modelName";
        $active = [];
        $inactive = [];
        $data = $model::query()->whereIn('id', $ids)->get();
        foreach ($data as $item){
            if($item->seo){
                $active[] = $item->id;
            }
            else {
                $inactive[] = $item->id;
            }
        }
        if(count($inactive) > 0){
            $model::query()->whereIn('id', $inactive)->update(['seo' => 1]);
        }
        if(count($active) > 0){
            $model::query()->whereIn('id', $active)->update(['seo' => 0]);
        }

        return back();
    }
}
