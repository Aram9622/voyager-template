<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuoteFormMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $shipFrom;
    public $shipTo;
    public $name;
    public $email;
    public $phone;
    public $time;
    public $shipping;
    public $operable;
    public $vehicle;
    public $ip;

    public function __construct($from,$to,$name,$email,$phone,$time,$shipping,$operable,$vehicle)
    {
        $this->shipFrom = $from;
        $this->shipTo = $to;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->time = $time;
        $this->shipping = $shipping;
        $this->operable = $operable;
        $this->vehicle = $vehicle;
        $this->ip = $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.agent-mail');
    }
}
