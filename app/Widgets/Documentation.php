<?php

namespace App\Widgets;

use App\Models\Post;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;

class Documentation extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = count(array_values(Post::query()
            ->get()
            ->filter(function ($item) {
                return $item->category()->first()->slug == 'blogs';
            })
            ->toArray()));
        $string = trans_choice('voyager::dimmer.user', $count);


        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-file-text',
            'title'  => "Documentation",
            'text'   => '',
            'button' => [
                'text' => 'Read More',
                'link' => '#',
            ],
            'image' => asset('images/bgi5.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}
