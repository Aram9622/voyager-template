<?php

namespace App\Repositories;

use App\Contracts\QuoteFormInterface;
use App\Mail\QuoteFormInfoMail;
use App\Mail\QuoteFormMail;
use Illuminate\Support\Facades\Mail;

class QuoteFormRepository implements QuoteFormInterface
{
    public function setDataToMailForm($data)
    {
        $from = $data['from'];
        $to = $data['to'];
        $name = $data['name'];
        $email = $data['email'];
        $phone = $data['phone'];
        $time = $data['time'];
        $shipping = $data['shipping'];
        $operable = $data['operable'];
        $vehicle = $data['vehicle'];

        try {
            Mail::to(env('MAIL_TO'))->send(
                new QuoteFormMail(
                    $from,
                    $to,
                    $name,
                    $email,
                    $phone,
                    $time,
                    $shipping,
                    $operable,
                    $vehicle
                )
            );

            Mail::to($email)->send(
                new QuoteFormInfoMail($name)
            );
        }catch (\Exception $exception){

        }
    }
}
