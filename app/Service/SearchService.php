<?php

namespace App\Service;

use App\Models\Page;
use App\Models\Post;
use App\Models\SearchParam;
use Illuminate\Support\Facades\Log;

class SearchService {
    public function dynamicQuery($data){
        $limit = 5;
        $page = $data['page'];
        $skip = $page == 1 ? 0 : $limit * $page - $limit;
        $searchText = ' '.$data['text'].' ';
        Log::error($searchText);
        $post = Post::query();
        $listData = [];
        $listData['empty'] = false;
        if(!empty(trim($searchText))){
            $listData['pages'] = Page::query()
                ->where('title', 'LIKE', '%'.trim($searchText).'%')
                ->orWhere('title', 'LIKE', trim($searchText).'%')
                ->skip($skip)
                ->take($limit)
                ->get(['id', 'slug', 'title']);
            if(count($listData['pages']) == 0){
                $listData['posts'] = Post::query()
                    ->with('category')
                    ->where('title', 'LIKE', '%'.$searchText.' %')
                    ->orWhere('title', 'LIKE', '%'.trim($searchText).'%')
                    ->orWhere('body', 'LIKE', '%'.$searchText.'%')
                    ->orWhere('body', 'LIKE', '%'.trim($searchText).'%')
                    ->skip($skip)
                    ->take($limit);
            }
            else if(count($listData['pages']) < 5){
                $listData['posts'] = Post::query()
                    ->with('category')
                    ->where('title', 'LIKE', '%'.$searchText.' %')
                    ->orWhere('title', 'LIKE', '%'.trim($searchText).'%')
                    ->orWhere('body', 'LIKE', '%'.$searchText.'%')
                    ->orWhere('body', 'LIKE', '%'.trim($searchText).'%')
                    ->skip(0)
                    ->take($limit - count($listData['pages']));
            }
            if(isset($listData['posts'])){
                $listData['posts'] = $listData['posts']->get(['id', 'title', 'category_id', 'body', 'image', 'slug'])
                    ->map(function ($item) use ($searchText){
//                        dd(strtolower($item->body));
                        if(strpos(strtolower($item->title), strtolower(trim($searchText)))){
                            $item->findField = 'title';
                        }
                        else if(strpos(strtolower($item->body), strtolower(trim($searchText)))){
                            $item->findField = 'body';
                        }
                        return $item;
                    });
            }
            $listData['postByCategory'] = Post::query()->with('category')->whereHas('category', function ($query) use ($searchText){
                $query->where('name', 'LIKE', '%'.trim($searchText).'%');
            })->skip($skip)->take($limit)->get(['id','title', 'slug', 'category_id', 'body']);
        }
        $listData['currentPage'] = $page;
        if(isset($listData['pages']) && count($listData['pages']) == 0
            && isset($listData['posts']) && count($listData['posts']) == 0
            && isset($listData['postByCategory']) && count($listData['postByCategory']) == 0){
            $listData['empty'] = true;
        }

        return $listData;
    }

    public function checkExistsFieldInModel($model, $field, $modelName){
        if(in_array($field, $model)){
            return $field;
        }else if(in_array($field.'_id', $model)){
            return $field.'_id';
        }
        return $modelName.'_'.$field;
    }

    public function getByTag($tag)
    {
        return Post::query()->with('post_tag')->whereHas('post_tag', function ($query) use ($tag){
            $query->where('name', 'LIKE', '%'.$tag.'%');
        })->get();
    }
}
