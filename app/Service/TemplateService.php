<?php

namespace App\Service;


use App\Helper\Helper;
use App\Models\Media;
use App\Models\MenuItem;
use App\Models\Page;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use App\Models\Menu;
use App\Helper\Template as TemplateHelper;

class TemplateService
{
    public function saveTemplate(Request $request, $id = null)
    {
        $template = [];
        $templateFields = [];
        $menuName = 'Front';
        $data = $request->except('_token', 'templateID', 'seo', 'featured', 'slug', 'slug_i18n', 'image_position');
        $errors = [];
        if (isset($data) && count($data) > 0) {
            foreach ($data as $item => $key) {
                $parentItem = explode(':', $item);

                $exp = explode('_', $parentItem[1]);

                if (is_array($key) && ($data['hidden:' . $exp[0] . '_allArray'] == 1)) {
                    foreach ($key as $index => $value) {

                        $template[$exp[0]][$index][$exp[1]] = $key[$index];
                        $templateFields[$exp[0]][$parentItem[0]][$index][$exp[1]] = $key[$index];
                    }
                } else {

                    if (is_array($key)) {
                        foreach ($key as $index => $value) {
                            $parentName = explode('|', $exp[1]);
                            $template[$exp[0]][$parentName[1]][$index][$parentName[0]] = $key[$index];
                            $templateFields[$exp[0]][$parentItem[0]][$parentName[1]][$index][$parentName[0]] = $key[$index];

                        }
                    } else {
                        if(TemplateHelper::ValidationTemplate($parentItem[0], $key) !== true){
                            $errors[$item] = TemplateHelper::ValidationTemplate($parentItem[0], $key);
                        }
                        $template[$exp[0]][$exp[1]] = $key;
                        $templateFields[$exp[0]][$parentItem[0]][$exp[1]] = $key;
                    }
                }
            }
        }
        if(count($errors) > 0){
            return back()->withErrors(['errorMessage' => $errors]);
        }
        $template = Helper::uploadFileMultiOrSingle($template);
        if(isset($template['error'])){

            return back()->with([
                'message'    => $template['error'],
                'alert-type' => 'error',
            ]);
        }
        $page = null;
        if($id){
            $page = Page::query()->find($id);
            $templateImages = json_decode($page->template_images, true);

            if(!empty($template['images'])){
                foreach(array_keys($template['images']) as $img){
                    if(is_array($template['images'][$img])){
                        foreach ($template['images'][$img] as $key => $imgValue) {
                            $templateImages[$img][$key] = $template['images'][$img][$key];
                        }
                    }
                    else {
                        $templateImages[$img] = $template['images'][$img];
                    }
                }

            }
        }
        else {
            $templateImages = $template['images'];
        }
        $pageTitle = $template['data'][array_keys($template['data'])[0]]['title'];
        $pageSavedData = [
            'template' => json_encode($template['data']),
            'template_fields' => json_encode($this->saveFieldsAndValuesWithTemplates($templateFields, $page ? $page->template_id : $request->templateID)),
            'template_images' => json_encode($templateImages),
            'template_id' => $page ? $page->template_id : $request->templateID,
            'title' => $pageTitle,
            'slug' => Str::slug($pageTitle, '-'),
            'seo' => $request->seo == 'on' ? 1 : 0,
            'status' => $request->featured == 'on' ? "ACTIVE" : "INACTIVE",
            'image_position' => $request->image_position
        ];

        $menuItemSavedData = [
            "menu_id" => Menu::query()->where('name', $menuName)->first()->id,
            "title" => $pageTitle,
            'url' => Str::slug($pageTitle, '-'),
            'order' => 1,
        ];


        Page::store($pageSavedData, $id);
        MenuItem::store($menuItemSavedData, $id);
        return redirect()->route('voyager.pages.index');
    }

    public function saveFieldsAndValuesWithTemplates($templateFields, $templateID)
    {
        $template = Template::query()->find($templateID);
        $templateJson = json_decode($template->fields, true);
        foreach ($templateFields as $key => $value) {
            foreach ($value as $type => $data) {
                $exp = explode('-', $type);
                if(count($exp) > 1){
                    foreach (array_keys($templateJson[$key][$exp[0]]) as $keysItem => $keysValue){
                        if(array_key_exists($keysItem, array_values($data[array_keys($data)[0]]))){
                            $deepArray = array_values($data[array_keys($data)[0]])[$keysItem][array_keys(array_values($data[array_keys($data)[0]])[$keysItem])[0]];
                            $templateJson[$key][$exp[0]][$keysItem][$exp[1]]['value'] = $deepArray;
                        }
                    }
                }
                else {
                    $templateJson[$key][$type]['value'] = $data[array_keys($data)[0]];
                }
            }
        }
        return $templateJson;
    }

    public function uploadTemplate(Request $request){
        $template = $request->except('_token');
        return Helper::uploadFileMultiOrSingle($template);
    }

    public function removeImage(Request $request){
        $imageName = $request->imageName;
        $parentImageKey = $request->parentImageKey;
        $pageID = $request->pageID;

        $page = Page::query()->find($pageID);
        $templateImages = json_decode($page->template_images, true);
        $template = json_decode($page->template, true);

        if($parentImageKey){
            unset($templateImages[$parentImageKey][$imageName]);
        }
        else {
            unset($templateImages[$imageName]);
        }
        $page->template_images = json_encode($templateImages);
        $page->save();
        return true;
    }

    public function updateMainPage(Request  $request, $page){
        $page = Page::query()->find($page);
        $data = $request->except(['_token', '_method', 'seo', 'featured', 'slug', 'slug_i18n', 'title', 'templateID']);
        $jsonData = json_encode($data);
        if($page){
            $page->template = $jsonData;
            $page->seo = $request->seo == 'on' ? 1 : 0;
            $page->status = $request->featured == 'on' ? 'ACTIVE' : 'INACTIVE';
            if(!Page::checkUniqSlug($request->slug)){
                $page->slug = $request->slug;
            }
            $page->save();


            return back()->with([
                'message'    => $page->title.' page successfully updated',
                'alert-type' => 'success',
            ]);
        }
        else {

            Page::query()->create([
                'title' => $request->title,
                'slug' => Str::slug($request->title),
                "template" => $jsonData,
                'template_id' => $request->templateID,
                "seo" => $request->seo == 'on' ? 1 : 0,
                "status" => $request->featured == 'on' ? 'ACTIVE' : 'INACTIVE',
            ]);
            $menuItemSavedData = [
                "menu_id" => Menu::query()->where('name', 'Front')->first()->id,
                "title" => $request->title,
                'url' => Str::slug($request->title, '-'),
                'order' => 1,
            ];


            MenuItem::store($menuItemSavedData);
            return redirect()->route('voyager.pages.index')->with([
                'message'    => $request->title.' page successfully created',
                'alert-type' => 'success',
            ]);
        }

    }

    public function getMainTemplateById(Page $page): \Illuminate\Http\JsonResponse
    {
        $template = isset($page->template) ? json_decode($page->template, true) : $page->template;
        return Helper::responseHandlerStatic('', 200, true, $template);
    }

    public function uploadMediaImages(Request $request){
        $data = $request->except(['_token']);
        $uploadSingleMedia = Helper::uploadOneImage($request, 'uploads');
        Media::query()->create(['file' => $uploadSingleMedia['fileLink']]);
        return Helper::responseHandlerStatic('', 200, true, $uploadSingleMedia);
    }

    public function refreshFromServer(): \Illuminate\Http\JsonResponse
    {
        $media = Media::query()->latest()->get();
        return Helper::responseHandlerStatic('', 200, true, $media);
    }


}
