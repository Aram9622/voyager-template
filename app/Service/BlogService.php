<?php

namespace App\Service;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogService
{
    public $model;

    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    public function get()
    {
        return $this->model->query()->with('category')->get();
    }

    public function getById($slug)
    {
        $post = $this->model
            ->query()
            ->with(['category', 'comment' => function($query){ $query->where('status', 1); },'post_tag'])
            ->where('slug', $slug)->first();
        $tagsIDs = array_column($post->post_tag->toArray(), 'id');
        $relatedPostsRow = DB::table('post_tags')->where('post_id', '<>', $post->id)->whereIn('tag_id', $tagsIDs)->get();
        $relatedPostsIds = $relatedPostsRow->pluck('post_id')->toArray();
        $relatedPosts = $this->model->with('category')->whereIn('id', $relatedPostsIds)->limit(3)->get(['id', 'image', 'updated_at', 'category_id', 'title', 'slug']);
        return [
            "post" => $post,
            "relatedPosts" => $relatedPosts
        ];
    }

    public function getByCategory($slug, $all, $limit = 12, $pageNumber = 1): array
    {
        $skip = $pageNumber == 1 ? 0 : $limit * $pageNumber - $limit;

        $posts = $this->model->query()->with(['category'])
            ->orderByDesc('id')
            ->get()
            ->filter(function ($item) use ($slug){
                return $item->category->slug == $slug;
            });
        if(!$all){
           $posts = $posts->skip($skip )
                ->take($limit);
        }
        return array_values($posts->toArray());


    }

    public function createComments($request){
        try {
            $post = $this->model->query()->findOrFail($request->id);
            $comment = $post->comment()->create($request->validated());

            $postComment = Post::find($request->id);
            $postComment->post_comment()->attach($comment->id);

            return "Comment successfully sent";
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }

    public function getLastDataWithArguments($data){
        if($data['category'] == 0 && $data['category'] !== null){
            return ['blogs' => $this->model->getTwoLastPost(1, $data['limit']), 'news' => $this->model->getTwoLastPost(2, $data['limit'])];
        }else {
            return $this->model->getTwoLastPost($data['category'], $data['limit']);
        }
    }
}
