<!--

createTag - create html tag. send properties ( field, name, placeholder, value, ... )

-->
<style>
    .image-wrapper {
        position: relative;
    }
    .change-bgi-position {
        position: absolute;
        right: -23px;
        top: calc(50% - 128px);
    }
    .change-bgi-position div {
        padding: 3px;
        text-align: center;
        cursor: pointer;
        border: 1px solid gray;
        margin-bottom: 6px;
        transition: .3s;
    }
    .change-bgi-position div:hover {
        background: lightgray;
        color: white;
    }
</style>

@php
$defaultImage = asset("/images/image.webp");
@endphp


@foreach ($templateView as $wrapper => $value)
    @foreach ($value as $field => $key)
        <!-- check is field is not string (  "text" => []  )  -->
        @if (!is_string($field))
            @foreach ($key as $item => $keyName)
                @if ($item == $TEMPLATE::TEXT)
                    {!! $TEMPLATE::createTag($item, $keyName['name'], $keyName['placeholder'], $keyName['value']) !!}
                @elseif ($item == $TEMPLATE::TEXTAREA)
                    {!! $TEMPLATE::createTag($item, $keyName['name'], $keyName['placeholder'], $keyName['value']) !!}
                @elseif ($item == $TEMPLATE::IMAGE)
                    <div class="container-fluid bgi"
                         style="border: 1px solid;padding: 56px 10px;text-align: center;">
                        {!! $TEMPLATE::createTag($TEMPLATE::IMAGE, $keyName['name'], $keyName['placeholder'], $TEMPLATE::checkArrayKeyExists($keyName, 'value')) !!}
                    </div>
                @elseif ($item == $TEMPLATE::BG_IMAGE)
                    <div class="container-fluid bgi"
                         style="border: 1px solid;padding: 56px 10px;text-align: center;">
                        <label for=""></label>
                        {!! $TEMPLATE::createTag($TEMPLATE::BG_IMAGE, $keyName['name'], $keyName['placeholder'], $TEMPLATE::checkArrayKeyExists($keyName, 'value')) !!}
                    </div>
                @elseif ($item == $TEMPLATE::LINK)
                    {!! $TEMPLATE::createTag($TEMPLATE::TEXT, $keyName['name'], $keyName['placeholder']) !!}
                @elseif ($item == $TEMPLATE::BUTTON)
                    {!! $TEMPLATE::createTag($TEMPLATE::TEXT, $keyName['name'], $keyName['placeholder']) !!}
                @endif
            @endforeach
        @else
            <!-- check is field is not string (  "text" => ""  )  -->
            @if ($field == $TEMPLATE::TEXT)
                @php
                    $value = array_key_exists('value', $key) ? $key['value'] : null;
                @endphp
                {!! $TEMPLATE::createTag($field, $key['name'], $key['placeholder'], $value) !!}
                @if($errors->any())
                    @if(array_key_exists($key['name'], $errors->getMessages()['errorMessage']))
                        <h6 class="text-danger">{{$errors->getMessages()['errorMessage'][$key['name']]}}</h6>
                    @endif
                @endif
            @elseif ($field == $TEMPLATE::TEXTAREA)
                @php
                    $value = array_key_exists('value', $key) ? $key['value'] : null;
                @endphp
                <div>
                    {!! $TEMPLATE::createTag($TEMPLATE::TEXTAREA, $key['name'], $key['placeholder'], $value) !!}
                    @if($errors->any())
                        @if(array_key_exists($key['name'], $errors->getMessages()['errorMessage']))
                            <h6 class="text-danger">{{$errors->getMessages()['errorMessage'][$key['name']]}}</h6>
                        @endif
                    @endif
                </div>
            @elseif ($field == $TEMPLATE::IMAGE)

                @php
                    $parentIMG = explode('_', $key['name'])[1];
                    $imageLink = isset($templateImages) ? $templateImages[$parentIMG] : $defaultImage;
                    $className = $imageLink == $defaultImage ? '' : 'showDeleteIcon';
                    $templateImage = !empty($TEMPLATE::checkArrayKeyExists($key, 'value')) ? $TEMPLATE::checkArrayKeyExists($key, 'value') : '';
                @endphp
                <div class="container-fluid bgi <?= $className?>"
                     style="border: 1px solid;padding: 56px 10px;text-align: center;background: url({{ $imageLink }}) no-repeat center/cover">
                    <div class="delete-icon"><i class="voyager-trash"></i></div>
                    <label for="<?= $key['name'] ?>"></label>
                    {!! $TEMPLATE::createTag($TEMPLATE::IMAGE, $key['name'], $key['placeholder'], $templateImage) !!}
                </div>
            @elseif ($field == $TEMPLATE::LINK)
                {!! $TEMPLATE::createTag($TEMPLATE::TEXT, $key['name'], $key['placeholder'], $key['value']) !!}
            @elseif ($field == $TEMPLATE::BG_IMAGE)
                @php

                    $parentIMG = explode('_', $key['name'])[1];
                    if(isset($templateImages) && array_key_exists($parentIMG, $templateImages)){
                        $imageLink = isset($templateImages) ? $templateImages[$parentIMG] : $defaultImage;
                    }
                    else {
                        $imageLink = $defaultImage;
                    }
                    $className = $imageLink == $defaultImage ? '' : 'showDeleteIcon';
                    $templateImage = !empty($TEMPLATE::checkArrayKeyExists($key, 'value')) ? $TEMPLATE::checkArrayKeyExists($key, 'value') : '';
                @endphp
                <div class="image-wrapper">
                    <div class="container-fluid bgi <?= $className?>"
                         style="border: 1px solid;padding: 56px 10px;text-align: center;background: url({{ $imageLink }}) no-repeat {{ !isset($dataTypeContent->image_position) ? "center" : explode(',',$dataTypeContent->image_position)[0].' '.explode(',',$dataTypeContent->image_position)[1]}}/cover">

                        <div class="delete-icon"><i class="voyager-trash"></i></div>
                        <label for="<?= $key['name'] ?>"></label>
                        {!! $TEMPLATE::createTag($TEMPLATE::IMAGE, $key['name'], $key['placeholder'], $templateImage, $key['name']) !!}
                    </div>
                    <div class="change-bgi-position">
{{--                        <div class="left icon voyager-angle-left"></div>--}}
                        <div class="top icon voyager-angle-up"></div>
{{--                        <div class="right icon voyager-angle-right"></div>--}}
                        <div class="bottom icon voyager-angle-down"></div>
                    </div>

                </div>


            @elseif ($field == $TEMPLATE::BUTTON)
                {!! $TEMPLATE::createTag($TEMPLATE::TEXT, $key['name'], $key['placeholder']) !!}
                <!-- check is field is not string (  "text" => "texts" or "texts1" or "texts2"  )  -->
                <!-- texts* is a array field  ( texts* => [ [] ] )  -->
            @elseif ($field == 'texts' || $field == 'texts1' || $field == 'texts2')
                <div class="row" style="margin:0">
                    @foreach ($key as $index => $list)
                        @if(isset($list[$TEMPLATE::TEXT]))
                            @php
                                $value = array_key_exists('value', $list[$TEMPLATE::TEXT]) ? $list[$TEMPLATE::TEXT]['value'] : null;
                            @endphp
                            {!! $TEMPLATE::createTag($TEMPLATE::TEXT, $list[$TEMPLATE::TEXT]['name'], $list[$TEMPLATE::TEXT]['placeholder'], $value) !!}
                            <div class="errorMessage"></div>
                        @endif
                        @if (isset($list[$TEMPLATE::LINK]))
                            @php
                                $value = array_key_exists('value', $list[$TEMPLATE::LINK]) ? $list[$TEMPLATE::LINK]['value'] : null;
                            @endphp
                            {!! $TEMPLATE::createTag($TEMPLATE::TEXT, $list[$TEMPLATE::LINK]['name'], $list[$TEMPLATE::LINK]['placeholder'], $value) !!}
                        @endif
                        @if (isset($list[$TEMPLATE::BUTTON]))
                            {!! $TEMPLATE::createTag($TEMPLATE::TEXT, $list[$TEMPLATE::BUTTON]['name'], $list[$TEMPLATE::BUTTON]['placeholder'], $list[$TEMPLATE::BUTTON]['value']) !!}
                        @endif
                        @if (isset($list[$TEMPLATE::TEXTAREA]))
                            @php
                                $value = array_key_exists('value', $list[$TEMPLATE::TEXTAREA]) ? $list[$TEMPLATE::TEXTAREA]['value'] : null;
                            @endphp
                            <div class="w-100">
                                {!! $TEMPLATE::createTag($TEMPLATE::TEXTAREA, $list[$TEMPLATE::TEXTAREA]['name'], $list[$TEMPLATE::TEXTAREA]['placeholder'], $value) !!}
                            </div>
                        @endif
                        @if (isset($list[$TEMPLATE::IMAGE]))
                            @php

                                $parentIMG = substr(explode('|', $list[$TEMPLATE::IMAGE]['name'])[1], 0, -2);

                                if(isset($templateImages) && count($templateImages) > 0  && array_key_exists($parentIMG, $templateImages) && array_key_exists($index, $templateImages[$parentIMG])){
                                    $imageLink = isset($templateImages) ? $templateImages[$parentIMG][$index][array_keys($templateImages[$parentIMG][$index])[0]] : $defaultImage;
                                }
                                else {
                                    $imageLink = $defaultImage;
                                }
                                $className = $imageLink == $defaultImage ? '' : 'showDeleteIcon';
                                    $templateImage = !empty($TEMPLATE::checkArrayKeyExists($list[$TEMPLATE::IMAGE], 'value')) ? $TEMPLATE::checkArrayKeyExists($list[$TEMPLATE::IMAGE], 'value') : '';
                            @endphp
                            <div class="col-md-<?= count($key) > 2 ? '3' : '6' ?> bgi px-0 <?= $className?>"
                                 style="border: 1px solid;padding: 56px 10px;text-align: center;background: url({{ $imageLink }}) no-repeat center/cover">
                                <div class="delete-icon"><i class="voyager-trash"></i></div>
                                <label for="{{ $wrapper . "image_$index" }}"></label>
                                {!! $TEMPLATE::createTag($TEMPLATE::IMAGE, $list[$TEMPLATE::IMAGE]['name'], $list[$TEMPLATE::IMAGE]['placeholder'], $templateImage, $wrapper . "image_$index") !!}
                            </div>
                        @endif
                        @if (isset($list[$TEMPLATE::BG_IMAGE]))
                            @php
                                $templateImage = count($TEMPLATE::checkArrayKeyExists($list[$TEMPLATE::IMAGE], 'value')) > 0 || !empty($TEMPLATE::checkArrayKeyExists($list[$TEMPLATE::IMAGE], 'value')) ? $TEMPLATE::checkArrayKeyExists($list[$TEMPLATE::IMAGE], 'value') : '';
                            @endphp
                            {!! $TEMPLATE::createTag($TEMPLATE::IMAGE, $list[$TEMPLATE::BG_IMAGE]['name'], $list[$TEMPLATE::BG_IMAGE]['placeholder'], $templateImage, $wrapper . "image_$index") !!}
                        @endif
                    @endforeach
                </div>
            @endif
        @endif
        <!-- we need hidden input for understanding well section is section hall array or half array -->
        @if ($field == $TEMPLATE::HIDDEN)
            {!! $TEMPLATE::createTag($TEMPLATE::HIDDEN, $key['name'], null, 0) !!}
            <input type="hidden" id="image-position-coordinate" name="image_position">
        @endif
    @endforeach
@endforeach


