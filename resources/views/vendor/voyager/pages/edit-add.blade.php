@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());

@endphp
{{--@inject('mediaQuery', "App\\Models\\Media")--}}
@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .panel .mce-panel {
            border-left-color: #fff;
            border-right-color: #fff;
        }

        .panel .mce-toolbar,
        .panel .mce-statusbar {
            padding-left: 20px;
        }

        .panel .mce-edit-area,
        .panel .mce-edit-area iframe,
        .panel .mce-edit-area iframe html {
            padding: 0 10px;
            min-height: 350px;
        }

        .mce-content-body {
            color: #555;
            font-size: 14px;
        }

        .panel.is-fullscreen .mce-statusbar {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 200000;
        }

        .panel.is-fullscreen .mce-tinymce {
            height: 100%;
        }

        .panel.is-fullscreen .mce-edit-area,
        .panel.is-fullscreen .mce-edit-area iframe,
        .panel.is-fullscreen .mce-edit-area iframe html {
            height: 100%;
            position: absolute;
            width: 99%;
            overflow-y: scroll;
            overflow-x: hidden;
            min-height: 100%;
        }

        .panel-file .upload-body {
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-bottom: 20px;
        }

        .panel-file .upload-body label {
            display: block;
            min-width: 150px;
            max-width: 300px;
            height: 37px;
            margin: 0;
        }

        .upload-body .clear-none {
            display: none;
        }

        .panel-file .image-information label {
            width: 100%;
        }

        .panel-file .image-information {
            display: none;
        }

        .bgi {
            background: url("/images/image.webp") no-repeat center/contain;
            margin-bottom: 20px;
            padding: 0 !important;
            height: 350px;

        }

        .bgi label {
            width: 100%;
            height: 100%;
            cursor: pointer;
        }

        .bgi input {
            display: none;
        }

        .form-group {
            /*width: 100%;*/
        }
    </style>
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        @if($isMainPage)
            Edit {{ $dataTypeContent->title }}
        @else
            {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataTypeContent->title }}
        @endif
    </h1>
    @if(!$edit)
        <small>As you add it will look like this.</small>
    @endif
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
{{--    {{ dd($dataTypeContent) }}--}}
    @include('widgets.loaderMosaic')
    @include('widgets.modal')
    @if(isset($templateView) && count($templateView) > 0 && $isMainPage == null && !isset($templateInfo))
        <div class="page-content-template container-fluid">
            <form action="{{ $edit ? route('template.service', $dataTypeContent->id) : route('template.service') }}"
                  method="POST" enctype="multipart/form-data"
                  data-pageid="{{ $dataTypeContent->id }}"
                  class="templateForm">
                <div class="row">
                    <div class="col-md-8">

                        @csrf
                        @if(isset($_GET['templateID']))
                            <input type="hidden" name="templateID" value="{{ $_GET['templateID'] }}">
                        @endif

                        <div class="card">
                            <div class="card-body">
                                @php
                                    $TEMPLATE = \App\Helper\Template::class;

                                @endphp
                                    <!-- Include Template library -->
                                @include('library.template')
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Save</button>

                    </div>
                    <div class="col-md-4">
                        <!-- ### DETAILS ### -->
                        <div class="panel panel panel-bordered panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i
                                        class="icon wb-clipboard"></i> {{ __('voyager::post.details') }}
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                       aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="slug">{{ __('voyager::post.slug') }}</label>
                                    @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'slug',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                    ])
                                    <input type="text" class="form-control" id="slug" name="slug"
                                           placeholder="slug"
                                           {!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}
                                           value="{{ $dataTypeContent->slug ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="featured">{{ __('voyager::post.status') }}</label>
                                    <input type="checkbox" name="featured"
                                           @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'ACTIVE') checked="checked"@endif>
                                </div>

                                <div class="form-group">
                                    <label for="seo">SEO</label>
                                    <input type="checkbox" name="seo"
                                           @if($dataTypeContent->seo) checked="checked"@endif>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif
    @if(isset($isMainPage) && $isMainPage == 1)
        <style>
            .details {
                position: fixed;
                right: 0;
                top: 150px;
                z-index: 9;
            }
        </style>
        <link rel="stylesheet" href="{{asset('css/'.$dataTypeContent->file_name.'.css')}}">

        <div class="page-content container-fluid">
            <form class="form-edit-add main-page" role="form"
                  action="{{route('template.updateMainPage', $dataTypeContent->id)}}"
                  method="POST" enctype="multipart/form-data" data-id="{{$dataTypeContent->id}}">
                <!-- PUT Method if we are editing -->

                {{ csrf_field() }}
                @method("PUT")
                @php
                    $fileName = $dataTypeContent->file_name;
                @endphp
                {{--                @include('layouts.mainFormTemplate')--}}

                <div class="row">
                    <div class="col-md-12">@include("layouts.mainPages.$fileName")</div>
                    <div class="details">
                        <!-- ### DETAILS ### -->
                        <div class="panel panel panel-bordered panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i
                                        class="icon wb-clipboard"></i> {{ __('voyager::post.details') }}
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="slug">{{ __('voyager::post.slug') }}</label>
                                    @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'slug',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                    ])
                                    <input type="text" class="form-control" id="slug" name="slug"
                                           placeholder="slug"
                                           {!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}
                                           value="{{ $dataTypeContent->slug ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="featured">Page Status</label>
                                    <input type="checkbox" name="featured"
                                           @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'ACTIVE') checked="checked"@endif>
                                </div>

                                <div class="form-group">
                                    <label for="seo">SEO</label>
                                    <input type="checkbox" name="seo"
                                           @if($dataTypeContent->seo) checked="checked"@endif>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group text-right">
                    <button class="btn btn-success" type="submit">Save</button>
                </div>
            </form>

            <div style="display:none">
                <input type="hidden" id="upload_url" value="{{ route('voyager.upload') }}">
                <input type="hidden" id="upload_type_slug" value="{{ $dataType->slug }}">
            </div>
        </div>
    @endif
    @if(!isset($_GET['templateID']) && !$edit)
        <style>
            .photo-gallery {
                color: #313437;
                background-color: #fff;
            }

            .photo-gallery p {
                color: #7d8285;
            }

            .photo-gallery h2 {
                font-weight: bold;
                margin-bottom: 40px;
                padding-top: 40px;
                color: inherit;
            }

            @media (max-width: 767px) {
                .photo-gallery h2 {
                    margin-bottom: 25px;
                    padding-top: 25px;
                    font-size: 24px;
                }
            }

            .photo-gallery .intro {
                font-size: 16px;
                max-width: 500px;
                margin: 0 auto 40px;
            }

            .photo-gallery .intro p {
                margin-bottom: 0;
            }

            .photo-gallery .photos {
                padding-bottom: 20px;
            }

            .photo-gallery .item {
                position: relative;
                overflow: hidden;
                transition: .3s;
            }

            .photo-gallery .item:hover {
                transform: scale(1.05);
            }

            .photo-gallery .item:hover .icon-zoom-in {
                bottom: 25px;
            }

            .item a {
                display: block;
                width: 100%;
                height: 100%;
            }

            .item a img {
                width: 100%;
                height: 100%;
                object-fit: cover;
                transition: .3s;
            }

            .icon-zoom-in {
                width: 50px !important;
                height: 50px !important;
                display: flex !important;
                justify-content: center;
                align-items: center;
                position: absolute;
                left: calc(50% - 25px);
                bottom: -50px;
                background: #22b2a9;
                box-shadow: 0 0 15px gray;
                border-radius: 50%;
                z-index: 9;
                text-decoration: none;
                transition: .3s;
            }

            .icon-zoom-in i {
                font-size: 24px;
                line-height: 20px;
                color: #fff;
            }

            .photos {
                column-count: 4;
                column-gap: 10px;
            }

            .photos .item {
                width: 100%;
                display: inline-block;
            }
        </style>
        <div class="photo-gallery">
            <div class="container">
                <div class="intro">
                    <h2 class="text-center">Template List</h2>
                    <p class="text-center">In this section showed templates for pages. Choose the template and fill your
                        imagine template</p>
                </div>
                <div class="row photos"
                     style="column-count: @if($templateList->links()->paginator->count() >= 4) {{ 4 }} @else {{ $templateList->links()->paginator->count() }} @endif">
                    @foreach($templateList as $list)
                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="{{route("voyager.pages.create")}}?templateID={{$list["id"]}}">
                                <img class="img-fluid" src="{{$list['image']}}">
                            </a>
                            <a href="{{$list['image']}}" data-lightbox="photos" class="icon-zoom-in">
                                <i class="voyager-photos"></i>
                            </a>
                        </div>
                    @endforeach
                </div>
                @if ($templateList->links()->paginator->hasPages())
                    <div class="mt-4 p-4 box has-text-centered">
                        {{ $templateList->links() }}
                    </div>
                @endif
            </div>
        </div>
    @endif
    @if(isset($templateInfo) && $templateInfo->type != 'page')
        <style>
            .details {
                position: fixed;
                right: 0;
                top: 150px;
                z-index: 9;
            }
        </style>
        <link rel="stylesheet" href="{{asset('css/'.$dataTypeContent->slug.'.css')}}">

        <div class="page-content container-fluid">
            <form class="form-edit-add main-page" role="form"
                  action="{{ $edit ? route('template.updateMainPage', $dataTypeContent->id) : route('template.updateMainPage', 0) }}"

                  method="POST" enctype="multipart/form-data" data-id="{{$dataTypeContent->id}}">
                <!-- PUT Method if we are editing -->

                {{ csrf_field() }}
                @method("PUT")
                @php
                    $fileName = $templateInfo->type;
                @endphp
                {{--                @include('layouts.mainFormTemplate')--}}
                @if(isset($_GET['templateID']))
                    <input type="hidden" name="templateID" value="{{ $_GET['templateID'] }}">
                @endif

                <div class="row">
                    <div class="col-md-12">@include("layouts.mainPages.services")</div>
                    <div class="details">
                        <!-- ### DETAILS ### -->
                        <div class="panel panel panel-bordered panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i
                                        class="icon wb-clipboard"></i> {{ __('voyager::post.details') }}
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="slug">{{ __('voyager::post.slug') }}</label>
                                    @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'slug',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                    ])
                                    <input type="text" class="form-control" id="slug" name="slug"
                                           placeholder="slug"
                                           {!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}
                                           value="{{ $dataTypeContent->slug ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="featured">Page Status</label>
                                    <input type="checkbox" name="featured"
                                           @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'ACTIVE') checked="checked"@endif>
                                </div>

                                <div class="form-group">
                                    <label for="seo">SEO</label>
                                    <input type="checkbox" name="seo"
                                           @if($dataTypeContent->seo) checked="checked"@endif>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group text-right">
                    <button class="btn btn-success" type="submit">Save</button>
                </div>
            </form>

            <div style="display:none">
                <input type="hidden" id="upload_url" value="{{ route('voyager.upload') }}">
                <input type="hidden" id="upload_type_slug" value="{{ $dataType->slug }}">
            </div>
        </div>
    @endif
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('#slug').slugify();

            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function () {
                            $(this).remove();
                        })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
