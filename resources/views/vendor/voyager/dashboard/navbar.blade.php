<nav class="navbar navbar-default navbar-fixed-top navbar-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="hamburger btn-link">
                <span class="hamburger-inner"></span>
            </button>
            @section('breadcrumbs')
            <ol class="breadcrumb hidden-xs">
                @php
                $segments = array_filter(explode('/', str_replace(route('voyager.dashboard'), '', Request::url())));
                $url = route('voyager.dashboard');
                $id = null;
                $dbName = null;
                @endphp
                @if(count($segments) == 0)
                    <li class="active"><i class="voyager-boat"></i> {{ __('voyager::generic.dashboard') }}</li>
                @else
                    <li class="active">
                        <a href="{{ route('voyager.dashboard')}}"><i class="voyager-boat"></i> {{ __('voyager::generic.dashboard') }}</a>
                    </li>
                    @foreach ($segments as $segment)
                        @php
                        $url .= '/'.$segment;
                        @endphp
                        @if ($loop->last)
                            @if(intval(ucfirst(urldecode($segment))))
                                @php
                                    $id = urldecode($segment);

                                    $dbname = explode('/', $_SERVER['REQUEST_URI'])[count(explode('/', $_SERVER['REQUEST_URI']))-2];
                                    $page = \Illuminate\Support\Facades\DB::table($dbname)->where('id', $id)->first();
                                    if(property_exists($page, 'slug')){
                                        $pageName = $page->slug;
                                    }
                                    else if(property_exists($page, 'name')){
                                        $pageName = $page->name;
                                    }
                                    else if(property_exists($page, 'title')){
                                        $pageName = $page->title;
                                    }
                                @endphp
                                <li>{{ $pageName }}</li>
                            @else
                                <li>{{ ucfirst(urldecode($segment)) }}</li>
                            @endif

                        @else

                            @if(intval(ucfirst(urldecode($segment))))
                                @php
                                    $id = intval(ucfirst(urldecode($segment)));
                                @endphp
                            @else
                                @php
                                    $dbName = urldecode($segment);
                                @endphp
                            @endif
                            @php
                                if(isset($id) && isset($dbName)){
                                    $pageName = \Illuminate\Support\Facades\DB::table($dbName)->where('id', $id)->first();
                                }
                            @endphp
                            @if(isset($id) && isset($dbName))
                                @if(intval(ucfirst(urldecode($segment))))
                                        @if(property_exists($pageName, 'slug'))
                                            <li>
                                                <a href="{{ $url }}">{{ $pageName->slug }}</a>
                                            </li>
                                        @elseif(property_exists($pageName, 'name'))
                                            <li>
                                                <a href="{{ $url }}">{{ $pageName->name }}</a>
                                            </li>
                                        @elseif(property_exists($pageName, 'title'))
                                            <li>
                                                <a href="{{ $url }}">{{ $pageName->title }}</a>
                                            </li>
                                        @endif

                                    @endif
                            @else
                                <li>
                                    <a href="{{ $url }}">{{ ucfirst(urldecode($segment)) }}</a>
                                </li>
                            @endif

                        @endif
                    @endforeach
                @endif
            </ol>
            @show
        </div>
        <ul class="nav navbar-nav @if (__('voyager::generic.is_rtl') == 'true') navbar-left @else navbar-right @endif">
            <li class="dropdown profile">
                <a href="#" class="dropdown-toggle text-right" data-toggle="dropdown" role="button"
                   aria-expanded="false"><img src="{{ $user_avatar }}" class="profile-img"> <span
                            class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-animated">
                    <li class="profile-img">
                        <img src="{{ $user_avatar }}" class="profile-img">
                        <div class="profile-body">
                            <h5>{{ Auth::user()->name }}</h5>
                            <h6>{{ Auth::user()->email }}</h6>
                        </div>
                    </li>
                    <li class="divider"></li>
                    <?php $nav_items = config('voyager.dashboard.navbar_items'); ?>
                    @if(is_array($nav_items) && !empty($nav_items))
                    @foreach($nav_items as $name => $item)
                    <li {!! isset($item['classes']) && !empty($item['classes']) ? 'class="'.$item['classes'].'"' : '' !!}>
                        @if(isset($item['route']) && $item['route'] == 'voyager.logout')
                        <form action="{{ route('voyager.logout') }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-block">
                                @if(isset($item['icon_class']) && !empty($item['icon_class']))
                                <i class="{!! $item['icon_class'] !!}"></i>
                                @endif
                                {{__($name)}}
                            </button>
                        </form>
                        @else
                        <a href="{{ isset($item['route']) && Route::has($item['route']) ? route($item['route']) : (isset($item['route']) ? $item['route'] : '#') }}" {!! isset($item['target_blank']) && $item['target_blank'] ? 'target="_blank"' : '' !!}>
                            @if(isset($item['icon_class']) && !empty($item['icon_class']))
                            <i class="{!! $item['icon_class'] !!}"></i>
                            @endif
                            {{__($name)}}
                        </a>
                        @endif
                    </li>
                    @endforeach
                    @endif
                </ul>
            </li>
        </ul>
    </div>
</nav>
