<a class="btn btn-primary" id="bulk_seo_btn" title=""><i class="voyager-diamond"></i> <span>Bulk SEO</span></a>

{{-- Bulk delete modal --}}
<div class="modal modal-info fade" tabindex="-1" id="bulk_seo_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-diamond"></i> Change SEO <span id="bulk_seo_count"></span> <span id="bulk_seo_display_name"></span>?
                </h4>
            </div>
            <div class="modal-body" id="bulk_seo_modal_body">
            </div>
            <div class="modal-footer">
                <form action="{{ route('voyager.'.$dataType->slug.'.seo.update') }}" id="bulk_seo_form" method="POST">
                    {{ method_field("PUT") }}
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_seo_input" value="">
                    <input type="hidden" name="model" value="{{$dataType->slug}}">
                    <input type="submit" class="btn btn-primary pull-right seo-confirm"
                           value="Yes">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('voyager::generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{{--<script>--}}
{{--    window.onload = function () {--}}
{{--        // Bulk delete selectors--}}
{{--        var $bulkSEOBtn = $('#bulk_seo_btn');--}}
{{--        var $bulkSEOModal = $('#bulk_seo_modal');--}}
{{--        var $bulkSEOCount = $('#bulk_seo_count');--}}
{{--        var $bulkSEODisplayName = $('#bulk_seo_display_name');--}}
{{--        var $bulkSEOInput = $('#bulk_seo_input');--}}
{{--        // Reposition modal to prevent z-index issues--}}
{{--        $bulkSEOModal.appendTo('body');--}}
{{--        // Bulk delete listener--}}
{{--        $bulkSEOBtn.click(function () {--}}
{{--            var ids = [];--}}
{{--            var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');--}}
{{--            var count = $checkedBoxes.length;--}}
{{--            if (count) {--}}
{{--                // Reset input value--}}
{{--                $bulkSEOInput.val('');--}}
{{--                // Deletion info--}}
{{--                var displayName = count > 1 ? '{{ $dataType->getTranslatedAttribute('display_name_plural') }}' : '{{ $dataType->getTranslatedAttribute('display_name_singular') }}';--}}
{{--                displayName = displayName.toLowerCase();--}}
{{--                $bulkSEOCount.html(count);--}}
{{--                $bulkSEODisplayName.html(displayName);--}}
{{--                // Gather IDs--}}
{{--                $.each($checkedBoxes, function () {--}}
{{--                    var value = $(this).val();--}}
{{--                    ids.push(value);--}}
{{--                })--}}
{{--                // Set input value--}}
{{--                $bulkSEOInput.val(ids);--}}
{{--                // Show modal--}}
{{--                $bulkSEOModal.modal('show');--}}
{{--            } else {--}}
{{--                // No row selected--}}
{{--                console.log('enter---')--}}
{{--                toastr.warning("You haven't selected anything to set seo");--}}
{{--            }--}}
{{--        });--}}
{{--    }--}}
{{--</script>--}}
