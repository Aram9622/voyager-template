<style>
    .point {
        width: 40px;
        height: 40px;
        border: 1px solid;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 20px;
        font-family: monospace;
    }

    #seoModal .col-md-6 > div, #seoModal .col-md-12 >div, #seoModal .col-md-4 >div {
        border: 1px solid;
        min-height: 102px;
        border-radius: 5px;
    }

    #seoModal .row {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }
    .box-success>div {
        border-color: #54b054 !important;
        border-width: 2px !important;
        padding: 10px;
    }
    .box-warning>div {
        border-color: darkorange !important;
        border-width: 2px !important;
        padding: 10px;
    }
    .box-danger>div {
        border-color: darkred !important;
        border-width: 2px !important;
        padding: 10px;
    }

    .box-info>div {
        border-color: lightblue !important;
        border-width: 2px !important;
    }
    #seoModal .col-md-4>div {
        display: flex;
        align-items: center;
    }
    .point.success {
        border: 3px solid green;
        background: #e0ffe0;
        color: green;
        font-weight: bold;
    }

    .point.warning {
        border: 3px solid orange;
        background: #fff4df;
        color: orange;
        font-weight: bold;
    }
    .point.danger {
        border: 3px solid red;
        background: #ffe8e8;
        color: red;
        font-weight: bold;
    }
    .collapse-title {
        display: flex;
        justify-content: space-between;
        border-bottom: 3px ridge;
        padding: 5px;
        cursor: pointer;
    }
    .collapse-title > span{
        transition: .25s;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .collapse-title p {
        margin: 0;
    }
    .collapse-body{
        height: 0;
        overflow: hidden;
        transition: .25s;
        min-height: 0;
        padding: 0 12px;
        background: #eeffee;
        margin-top: 2px;
    }

    .collapse-box.active  span{
        transform: rotate(180deg);
    }
    .collapse-box.active .collapse-body{
        min-height:auto;
        height: auto;
        border-bottom: 1px solid;
        max-height: 100%;
    }
    .collapse-box {
        margin-bottom: 7px;
    }
    .box-warning .collapse-body {
        background: #fff4df !important;
    }

    .box-danger .collapse-body {
        background: #ffe8e8 !important;
    }

    .header-point {
        display: flex;
        gap: 20px;
    }
</style>
<div class="modal fade bd-seo-modal-xl" id="seoModal" tabindex="-1" role="dialog" aria-labelledby="seoModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div style="display: flex; justify-content: space-between">
                    <div class="header-point">
                        <h2 class="modal-title" id="exampleModalLabel">SEO</h2>
                        <div class="point"></div>
                    </div>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

            </div>
            <div class="modal-body p-0 " style="overflow: hidden">
                <div class="row">
                    <div class="col-md-12 box-success">
                        <h4>PASSED AUDITS (<span class="count"></span>)</h4>
                        <div class="content">

                        </div>
                    </div>
                    <div class="col-md-6 box-warning">
                        <h4>NOT APPLICABLE (<span class="count"></span>)</h4>
                        <div class="content">
                        </div>
                    </div>
                    <div class="col-md-6 box-danger">
                        <h4>CONTENT BEST PRACTICES (<span class="count"></span>)</h4>
                        <div class="content">
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
