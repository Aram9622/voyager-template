<style>
    .img-wrapper {
        position: relative;
        height: 100%;
        margin-top: 15px;
    }

    .img-wrapper img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .img-overlay {
        background: rgba(0, 0, 0, 0.7);
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        opacity: 0;
    }

    .img-overlay i {
        color: #fff;
        font-size: 3em;
    }

    #overlay {
        background: rgba(0, 0, 0, 0.7);
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 999;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    #overlay img {
        margin: 0;
        width: 80%;
        height: auto;
        -o-object-fit: contain;
        object-fit: contain;
        padding: 5%;
    }

    @media screen and (min-width: 768px) {
        #overlay img {
            width: 60%;
        }
    }

    @media screen and (min-width: 1200px) {
        #overlay img {
            width: 80%;
        }
    }

    #nextButton {
        color: #fff;
        font-size: 2em;
        transition: opacity 0.8s;
    }

    #nextButton:hover {
        opacity: 0.7;
    }

    @media screen and (min-width: 768px) {
        #nextButton {
            font-size: 3em;
        }
    }

    #prevButton {
        color: #fff;
        font-size: 2em;
        transition: opacity 0.8s;
    }

    #prevButton:hover {
        opacity: 0.7;
    }

    @media screen and (min-width: 768px) {
        #prevButton {
            font-size: 3em;
        }
    }

    #exitButton {
        color: #fff;
        font-size: 2em;
        transition: opacity 0.8s;
        position: absolute;
        top: 15px;
        right: 15px;
    }

    #exitButton:hover {
        opacity: 0.7;
    }

    @media screen and (min-width: 768px) {
        #exitButton {
            font-size: 3em;
        }
    }

    .modal-gallery #image-gallery .row {
        height: 675px;
        overflow: auto;
    }

    .img-overlay {
        transition: 0.5s ease;
    }

    .img-overlay {
        opacity: 0.5;
    }

    .overlay-actions {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        display: flex;
        gap: 20px;
    }

    .actions-icons {
        color: white;
        font-size: 60px;
        cursor: pointer;
        line-height: 28px;
        display: none;
        background: #305b62;
        padding: 3px;
        border-radius: 50%;
    }
    .actions-icons > div:before {
        color: #ffffff;
    }
    #image-gallery .row {
        display: flex;
        align-items: flex-start;
        align-content: baseline;
        flex-wrap: wrap;
    }

    #image-gallery .row .image {
        height: 194px;
    }

    #image-gallery .row .image:hover .actions-icons {
        display: block;
    }

    .image-info-section {
        list-style: none;
        font-size: 20px;
        color: wheat;
    }

    .header-info {
        font-weight: bold;
    }

    .dropzone {
        width: 50%;
        margin: 1%;
        border: 2px dashed #3498db !important;
        border-radius: 5px;
    }



    #image-gallery .row::-webkit-scrollbar {
         width: 10px;
     }

    #image-gallery .row::-webkit-scrollbar-track {
         border-radius: 10px;
         background: #c9cacc;
     }

    #image-gallery .row::-webkit-scrollbar-track:hover {
         background: #78797a;
     }

    #image-gallery .row::-webkit-scrollbar-thumb {
         background: #3d5585;
         border-radius: 10px;
     }

    #image-gallery .row::-webkit-scrollbar-thumb:hover {
         background: #01143b;
     }

</style>
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css"/>
<div class="modal fade bd-example-modal-xl" id="exampleModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width: 1400px">
        <div class="modal-content">
            <div class="modal-header">
                <div style="display: flex; justify-content: space-between">
                    <h2 class="modal-title" id="exampleModalLabel">Media Library</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 35px">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

            </div>
            <div class="modal-body p-0">
                <div class="modal-gallery">
                    <section id="gallery-media">
                        <div class="container" style="padding: 0; margin: 0">
                            <div id="image-gallery">
                                <div class="row">
                                    @foreach($media as $item)
                                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 image">
                                            <div class="img-wrapper">
                                                <a data-href="{{ $item->file }}" href="{{ url($item->file) }}">
                                                    <img src="{{ url($item->file) }}" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="overlay-actions">
                                                <div class="actions-icons showImage">
                                                    <div class="icon voyager-eye"></div>
                                                </div>
                                                <div class="actions-icons chooseImage">
                                                    <div class="icon voyager-plus"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div><!-- End row -->
                            </div><!-- End image gallery -->
                        </div><!-- End container -->
                        <div class="wrapper" style="visibility:hidden; opacity:0">DROP HERE</div>
                        <form action="{{route('uploadMediaImages')}}" class="dropzone" id="dropzone" style="margin-left: 0">
                            @csrf
                            <div class="dz-message ">
                              <span class="text">
                              Drop files here or click to upload +
                              </span>
                            </div>
                        </form>
                        <button class="btn btn-success" id="refresh-from-server">Refresh from server</button>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
    let url = 'http://192.168.5.153:8000';
    let localUrl = "http://localhost:8001";
    console.log(location.origin)
    if(location.origin !== url && location.origin !== localUrl){
        url = 'https://austinmerchantservices.biz/example/washington/public';
    }
    if(location.origin === localUrl){
        url = localUrl
    }
    Dropzone.options.myDropzone = {
        // Configuration options go here
    };

    // Lightbox
    var $overlay = $('<div id="overlay"></div>');
    var $image = $("<div style='width: 60%'><img style='width: 100%; padding: 0'></div>");
    var $prevButton = "";
    var $nextButton = "";
    var $exitButton = "";

    // Add overlay
    $overlay.append($image).append(`
        <div style="width: 30%; height: 498px">
            <ul class="image-info-section">
                <li class="title-info"><span class="header-info">Title:</span> <span class="content-info"></span></li>
                <li class="alt-info"><span class="header-info">ALt:</span> <span class="content-info"></span></li>
                <li class="caption-info"><span class="header-info">Caption:</span> <span class="content-info"></span></li>
                <li class="description-info"><span class="header-info">Description:</span> <span class="content-info"></span></li>
            </ul>
        </div>
    `);
    $("#gallery-media").append($overlay);

    // Hide overlay on default
    $overlay.hide();

    // When an image is clicked
    $(".showImage").click(function (event) {

        // Prevents default behavior
        event.preventDefault();
        // Adds href attribute to variable
        var imageLocation = $(this).closest('.image').find('.img-wrapper a').attr("href");
        $.get({
            url: "{{route('imagePath')}}",
            data: {path: imageLocation},
            success: function (res) {
                $('.title-info .content-info').html(`${res.title}`);
                $('.alt-info .content-info').html(`${res.alt}`);
                $('.caption-info .content-info').html(`${res.caption}`);
                $('.description-info .content-info').html(`${res.description}`);
            }
        });
        // Add the image src to $image
        $image.find('img').attr("src", imageLocation);
        // Fade in the overlay
        $overlay.fadeIn("slow");
    });

    // When the overlay is clicked
    $overlay.click(function () {
        // Fade out the overlay
        $(this).fadeOut("slow");
    });

    // When next button is clicked


    $(document).on('click','.chooseImage',function (e) {
        let img = $(this).closest('.image').find('.img-wrapper a');
        let imageUrl = img.data('href')
        console.log($(this).closest('.image').find('.img-wrapper a'), 'imageeeee')
        let ID = $(this).data('id')
        $.get({
            url: "{{route('imagePath')}}",
            data: {path: imageUrl},
            success: function (res) {
                $('.image-information #titleImage').val(`${res.title}`);
                $('.image-information #alt').val(`${res.alt}`);
                $('.image-information #caption').val(`${res.caption}`);
                $('.image-information #description').val(`${res.description}`);
                $('.image-information #fileUrl').val(`${res.file}`);
                $(`.image-media-upload[name=${ID}]`).val(`${res.file}`);

                $('#exampleModal').modal('hide')


                if ($('.panel-file').find('img').hasClass('exists-image')) {
                    console.log('1')
                    $('.exists-image').attr('src', img.attr('href'));
                } else {
                    console.log('2')
                    $('#pcimage').removeClass('d-none');
                    $('#pcimage').css('display', 'block');
                    $('#pcimage').attr('src', imageUrl)
                    $(`.image-media-upload[name=${ID}]`).parent().find('.image-show-media').attr('src', img.attr('href'))
                }
            }
        })
        // $('#pcimage').show()
        $('#mediaImage').val(imageUrl)
    })


    // const getBlobFromUrl = (myImageUrl) => {
    //     return new Promise((resolve, reject) => {
    //         let request = new XMLHttpRequest();
    //         request.open('GET', myImageUrl, true);
    //         request.responseType = 'blob';
    //         request.onload = () => {
    //             resolve(request.response);
    //         };
    //         request.onerror = reject;
    //         request.send();
    //     })
    // }
    //
    // const getDataFromBlob = (myBlob) => {
    //     return new Promise((resolve, reject) => {
    //         let reader = new FileReader();
    //         reader.onload = () => {
    //             resolve(reader.result);
    //         };
    //         reader.onerror = reject;
    //         reader.readAsDataURL(myBlob);
    //     })
    // }
    //
    // const convertUrlToImageData = async (myImageUrl) => {
    //     try {
    //         let myBlob = await getBlobFromUrl(myImageUrl);
    //         // console.log(myBlob)
    //         let myImageData = await getDataFromBlob(myBlob);
    //         // console.log(myImageData)
    //         return myImageData;
    //     } catch (err) {
    //         console.log(err);
    //         return null;
    //     }
    // }
    function setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

</script>
<!-- Dropzone -->
<script>
    var lastTarget = null;
    // uploadMultiple
    window.addEventListener("dragenter", function (e) { // drag start
        console.log('dragenter')
        // unhide our red overlay
        showWrapper();
        lastTarget = e.target; // cache the last target here
    });

    window.addEventListener("dragleave", function (e) { // user canceled
        console.log('dragleave')
        if (e.target === lastTarget || e.target === document) {
            hideWrapper();
        }

    });

    window.addEventListener("dragover", function (e) { //to stop default browser act
        console.log('dro')
        e.preventDefault();
    });

    window.addEventListener("drop", function (e) {

        e.preventDefault();
        hideWrapper();
        console.log('drop')
        // if drop, we pass object file to dropzone
        var myDropzone = Dropzone.forElement(".dropzone");
        myDropzone.handleFiles(e.dataTransfer.files).then((res)=>{
            console.log(res)
        });

    });


    function hideWrapper() {
        document.querySelector(".wrapper").style.visibility = "hidden";
        document.querySelector(".wrapper").style.opacity = 0;
    }

    function showWrapper() {
        document.querySelector(".wrapper").style.visibility = "";
        document.querySelector(".wrapper").style.opacity = 0.5;
    }
</script>

<script>
    $('#refresh-from-server').on('click', function (){
        $.get({
            url: url+'/admin/refresh-from-server',
            success: function (res) {
                let outputSpace = $('#image-gallery .row');
                outputSpace.html('')
                let files = res.data;
                Dropzone.forElement('.dropzone').removeAllFiles(true)
                files.forEach((item)=>{
                    outputSpace.append(`
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 image">
                            <div class="img-wrapper">
                                <a href="${url}${item.file}" data-href="${item.file}">
                                    <img src="${url}${item.file}" class="img-responsive">
                                </a>
                            </div>
                            <div class="overlay-actions">
                                <div class="actions-icons showImage">
                                    <div class="icon voyager-eye"></div>
                                </div>
                                <div class="actions-icons chooseImage">
                                    <div class="icon voyager-plus"></div>
                                </div>
                            </div>
                        </div>
                `);
                })

            }
        })
    })
</script>
