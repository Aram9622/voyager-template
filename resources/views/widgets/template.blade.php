

<div class="slider-for">
    <div><img width="200px" src="https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg" alt=""></div>
    <div><img width="200px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTPRiMEs-_5W8ToxTkjYJgSuo5fX1xSILPwg&usqp=CAU" alt=""></div>
    <div><img width="200px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShPO1mzYNfCrAyaqJB-G6e9Q99YQ4zYuJS9A&usqp=CAU" alt=""></div>
    <div><img width="200px" src="https://s3.amazonaws.com/images.seroundtable.com/google-images-1548419288.jpg" alt=""></div>
    <div><img width="200px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQenPXGo_SejeSScDETTUV1JFCyN1sDYKrwBg&usqp=CAU" alt=""></div>
    <div><img width="200px" src="https://static01.nyt.com/images/2023/01/24/multimedia/google-layoffs-03-lwjt/google-layoffs-03-lwjt-videoSixteenByNine3000.jpg" alt=""></div>
</div>

<div class="slider-nav">
    <div><img width="200px" src="https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg" alt=""></div>
    <div><img width="200px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTPRiMEs-_5W8ToxTkjYJgSuo5fX1xSILPwg&usqp=CAU" alt=""></div>
    <div><img width="200px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShPO1mzYNfCrAyaqJB-G6e9Q99YQ4zYuJS9A&usqp=CAU" alt=""></div>
    <div><img width="200px" src="https://s3.amazonaws.com/images.seroundtable.com/google-images-1548419288.jpg" alt=""></div>
    <div><img width="200px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQenPXGo_SejeSScDETTUV1JFCyN1sDYKrwBg&usqp=CAU" alt=""></div>
    <div><img width="200px" src="https://static01.nyt.com/images/2023/01/24/multimedia/google-layoffs-03-lwjt/google-layoffs-03-lwjt-videoSixteenByNine3000.jpg" alt=""></div>
</div>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        centerMode: true,
        focusOnSelect: true
    });
</script>
