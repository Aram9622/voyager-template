<div class="transportation m-padding parent"  >
    <hr class="hr mb-60"/>
    <div class="mainContent">
        <div class="contentItem">
            <input type="text" class="font_20 blue" value=""/>
            <textarea rows="5" cols="30" class="blue form-control"></textarea>
            <textarea rows="5" cols="30" class="blue form-control"></textarea>
            <textarea rows="5" cols="30" class="blue form-control"></textarea>
            <textarea rows="5" cols="30" class="blue form-control"></textarea>
        </div>
        <div class="contentItem cards">
            <div class="transportCard">
                <input type="text" class="font_20 bold blue" value=""/>
                <textarea cols="50" rows="5" style="resize: none"></textarea>
                <div class="flex-between align-center">
                    <input type="text" class="white font-20 bold" value=""/>

                </div>
            </div>
            <div class="transportCard">
                <input type="text" class="font_20 bold blue" value=""/>
                <textarea cols="50" rows="5" style="resize: none"></textarea>
                <div class="flex-between align-center">
                    <input type="text" class="white font-20 bold" value=""/>

                </div>
            </div>
        </div>
        <div class="contentItem cards">
            <div class="transportCard">
                <input type="text" class="font_20 bold blue" value=""/>
                <textarea cols="50" rows="5" style="resize: none"></textarea>
                <div class="flex-between align-center">
                    <input type="text" class="white font-20 bold" value=""/>

                </div>
            </div>
            <div class="transportCard">
                <input type="text" class="font_20 bold blue" value=""/>
                <textarea cols="50" rows="5" style="resize: none"></textarea>
                <div class="flex-between align-center">
                    <input type="text" class="white font-20 bold" value=""/>

                </div>
            </div>
        </div>
    </div>
</div>
<section class="customersSection parent">
    <div class="customersSays">
        <div class="content">
            <hr class="hr">
            <input type="text" class="font_20 blue" placeholder="What Our Customers Say About Us">
            <textarea></textarea>
            <textarea></textarea>
            <textarea></textarea>
            <div>
                <label for="buttonText">Button text:</label>
                <input type="text" id="buttonText" placeholder="Button text">
            </div>
            <div>
                <label for="buttonLink">Link:</label>
                <input type="text" id="buttonLink" placeholder="Link">
            </div>
        </div>
        <div class="customersReviewImg">
            <input type="text" readonly placeholder="choose image from media" class="image-media-upload">
        </div>
        <div class="customersImg">
            <input type="text" readonly placeholder="choose image from media" class="image-media-upload">
        </div>
    </div>
</section>
<div class="commercialServices m-padding parent">
    <hr class="hr">
    <input type="text" class="font_20 blue bolder" value="Commercial Services">
    <div class="flex-center container">
        <div class="commercialCard">
            <input type="text" class="font-18 red" value="">
            <textarea class="font-16 blue"></textarea>
            <div>
                <input type="text" placeholder="Button text">
                <input type="text" placeholder="Link">
            </div>
        </div>
        <div class="commercialCard">
            <input type="text" class="font-18 red" value="">
            <textarea class="font-16 blue"></textarea>
            <div>
                <input type="text" placeholder="Button text">
                <input type="text" placeholder="Link">
            </div>
        </div>
        <div class="commercialCard">
            <input type="text" class="font-18 red" value="">
            <textarea class="font-16 blue"></textarea>
            <div>
                <input type="text" placeholder="Button text">
                <input type="text" placeholder="Link">
            </div>
        </div>
        <div class="commercialCard">
            <input type="text" class="font-18 red" value="">
            <textarea class="font-16 blue"></textarea>
            <div>
                <input type="text" placeholder="Button text">
                <input type="text" placeholder="Link">
            </div>
        </div>
    </div>
</div>
