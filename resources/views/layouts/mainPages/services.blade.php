<style>
    .vehicleAdvertising {
        padding: 40px 14.5% 60px 14.5%;
        /*background-color: blue;*/
    }

    .vehicleAdvertising > div {
        display: flex;
        justify-content: space-between;
        gap: 7.5%;
        position: relative;
    }

    .vehicleAdvertising > .hr {
        position: absolute;
        left: -14%;
        top: -20%;
    }
    .tox-tinymce {
        width: 580px !important;
    }


    .vehicleAdvertising .img {
        max-width: 460px;
        max-height: 340px;
    }

    img {
        border-radius: 10px;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }


    .content h1, p {
        margin-bottom: 25px;
        line-height: 38px;
    }

    .content .MuiButtonBase-root {
        width: 100%;
        max-width: 225px;
        height: 38px;
        background-color: red;
        border-radius: 10px;
    }


    .mPadding {
        padding-left: 18.2%;
        padding-right: 18.2%;
    }

    .mb20 {
        margin-bottom: 20px;
    }

    .mb30 {
        margin-bottom: 30px;
    }

    .mb40 {
        margin-bottom: 40px;
    }

    .flexBetween {
        display: flex;
        justify-content: space-between;
    }
</style>
@php

    $inputName = "service";
@endphp
<div class="page-main commercialVehicleTransportPage">
    <div class="vehicleAdvertising">
        @if($edit)
            <input type="text" class="white bolder font32 textCenter mb40" name="{{$inputName}}_title">
        @else
            {{--            <input type="text" class="white bolder font32 textCenter mb40" placeholder="" name="title" id="title">--}}

            <div class="panel-body">
                <input type="text" class="form-control" id="title" name="title"
                       placeholder="{{ __('voyager::generic.title') }}"
                       required
                       value="{{ $dataTypeContent->title ?? '' }}">
            </div>

            <input type="hidden" name="{{$inputName}}_title" id="service_title">
        @endif

        <div>
            {{--            <hr class="hr">--}}
            <div class="img">
                <input type="text" class="image-media-upload" readonly name="{{$inputName}}_image">
            </div>
            <div class="content">
                <input type="text" class="white bolder font32" placeholder="" name="{{$inputName}}_subTitle">
                <textarea class="white font20"
                          name="{{$inputName}}_shortDescription"
                          placeholder="" id="{{$inputName}}_shortDescription"></textarea>
                <div>
                    <input type="text" placeholder="Button text" name="{{$inputName}}_button">
                    <input type="text" placeholder="Button Link" name="{{$inputName}}_link">
                </div>
            </div>
        </div>
    </div>
    <div class="content flexBetween mPadding">
        <div>
            <textarea name="{{$inputName}}Left_description" id="{{$inputName}}Right_description"
                      class="blue font24 mb30" placeholder=""></textarea>
        </div>
        <div>
            <textarea name="{{$inputName}}Right_description" id="{{$inputName}}Left_description"
                      class="blue font24 mb30" placeholder=""></textarea>
        </div>
    </div>
</div>

<script>
    $('#title').on('keyup', function () {
        $('#service_title').val($(this).val())
    })
</script>
