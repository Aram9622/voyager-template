<div class="page-main careerPage" style="padding: 0 18%;">
    <div class="ourReviews mPadding flexBetween" style=" display: flex; justify-content: space-between; gap: 4%; align-items: center">
        <div class="content m60" style="margin-bottom: 60px; width: 48%" >
            <hr class="hr"/>
            <input type="text" name="career_title" style="margin-bottom: 40px;" placeholder="Career"/>
            <textarea name="career_text" id="career_text"></textarea>
        </div>
        <div class="imageArea" style="width: 48%">
            <input type="text" readonly class="image-media-upload" name="career_image">
        </div>
    </div>
    <div class="mainContent mPadding">
        <input type="text" name="career_secondTitle" placeholder="Title"/>
        <textarea name="career_secondText" id="career_secondText"></textarea>
        <input type="text" placeholder="Button text" name="career_buttonText" />
        <input type="text" placeholder="Link" name="career_buttonLink"/>
    </div>
</div>
