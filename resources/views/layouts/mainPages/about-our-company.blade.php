<main class="page-main mPadding companyPage">
    <div class="hrField">
        <hr class="hr">
    </div>
    <h1 class="font24 blue"><input type="text" name="aboutUs_title"></h1>
    <div class="companyContent">
        <textarea placeholder="top text" id="aboutUs_topText" name="aboutUs_topText" style="width: 1439px; height: 96px;"></textarea>
        <div class="img">
            <input type="text" name="aboutUs_image" readonly class="image-media-upload">
        </div>
        <div class="subTopic">
            <textarea placeholder="bottom text" id="aboutUs_bottomText" name="aboutUs_bottomText"></textarea>
        </div>
    </div>
    <div>
        <div class="red bold font16 titleSection">
            <input type="text" name="aboutUs_link_title" id="aboutUs_link_title" placeholder="link title">
            <input type="text" name="aboutUs_link_url" id="aboutUs_link_url" placeholder="link url"><span class="icon-chevron-thin-right iconArrow"></span></div>
        <div class="pricingPart">
            <textarea placeholder="about Us pricingText" id="aboutUs_pricingText" name="aboutUs_pricingText" style="width: 1441px; height: 155px;"></textarea>
        </div>
    </div>
</main>
