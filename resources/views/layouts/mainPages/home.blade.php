
<div class="transportation m-padding">
    <hr class="hr mb-60">
    <div class="mainContent">
        <div class="contentItem">
            <input type="text" name="transportation_title" class="font_20 blue" placeholder="Transportation Title">
            <textarea name="transportation_text" rows="10" class="blue" placeholder="Transportation Text" id="transportation_text"></textarea>
        </div>
        <div class="contentItem cards">
            <div class="transportCard">
                <input type="text" name="transportCard_title_0" class="font_20 bold blue" placeholder="Transport Card Title">
                <textarea name="transportCard_text_0" placeholder="Transport Card Text" id="transportCard_text_0"></textarea>
                <div class="flex-between align-center">
                    <input type="text" name="transportCard_button_0" class="white font-20 bold" placeholder="Button Text">
                    <input type="text" name="transportCard_link_0" class="white font-20 bold" placeholder="Link">
                </div>
            </div>
            <div class="transportCard">
                <input type="text" name="transportCard_title_1" class="font_20 bold blue" placeholder="Transport Card Title">
                <textarea name="transportCard_text_1" placeholder="Transport Card Text" id="transportCard_text_1"></textarea>
                <div class="flex-between align-center">
                    <input type="text" name="transportCard_button_1" class="white font-20 bold" placeholder="Button Text">
                    <input type="text" name="transportCard_link_1" class="white font-20 bold" placeholder="Link">
                </div>
            </div>
        </div>
        <div class="contentItem cards">
            <div class="transportCard">
                <input type="text" name="transportCard_title_2" class="font_20 bold blue" placeholder="Transport Card Title">
                <textarea name="transportCard_text_2" placeholder="Transport Card Text" id="transportCard_text_2"></textarea>
                <div class="flex-between align-center">
                    <input type="text" name="transportCard_button_2" class="white font-20 bold" placeholder="Button Text">
                    <input type="text" name="transportCard_link_2" class="white font-20 bold" placeholder="Link">
                </div>
            </div>
            <div class="transportCard">
                <input type="text" name="transportCard_title_3" class="font_20 bold blue" placeholder="Transport Card Title">
                <textarea name="transportCard_text_3" placeholder="Transport Card Text" id="transportCard_text_3"></textarea>
                <div class="flex-between align-center">
                    <input type="text" name="transportCard_button_3" class="white font-20 bold" placeholder="Button Text">
                    <input type="text" name="transportCard_link_3" class="white font-20 bold" placeholder="Link">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-padding calculationInfo" >
    <input type="text" class="mb-20 blue font_20 bolder" name="calculationPhase_title" placeholder="How we calculated your transport fee">
    <div class="flex-between calculationPhases">
        <div class="calculationPhase">
            <textarea class="blue bold font-18" name="calculationPhase_0" placeholder="The size and weight of your vehicle" id="calculationPhase_0"></textarea>
        </div>
        <div class="calculationPhase distancePhase">
            <textarea class="blue bold font-18" name="calculationPhase_1" placeholder="Distance between vehicle pickup and delivery" id="calculationPhase_1"></textarea>
        </div>
        <div class="calculationPhase">
            <textarea class="blue bold font-18" name="calculationPhase_2" placeholder="Choosing open or enclosed car transport" id="calculationPhase_2"></textarea>
        </div>
        <div class="calculationPhase">
            <textarea class="blue bold font-18" name="calculationPhase_3" placeholder="The condition of your vehicle" id="calculationPhase_3"></textarea>
        </div>
    </div>
</div>
<section class="customersSection parent">
    <div class="customersSays m-padding">
        <div class="content">
            <hr class="hr">
            <input type="text" class="font_20 blue" name="customersSays_title" />
            <textarea name="customersSays_text" id="customersSays_text"></textarea>
        </div>
        <div class="customersReviewImg">
            <input type="text"  name="customersSays_img_0" readonly class="image-media-upload" />
        </div>
        <div class="customersImg">
            <input type="text" name="customersSays_img_1" readonly class="image-media-upload" />
        </div>
    </div>
</section>
<div class="commercialServices m-padding">
    <hr class="hr">
    <input class="font_20 blue bolder" name="commercialServices_title" type="text">
    <div class="flex-start container">
        <div class="commercialCard undefined">
            <input class="font-18 red" name="commercialCard_title_0" type="text">
            <textarea class="font-16 blue" name="commercialCard_text_0" id="commercialCard_text_0"></textarea>
            <div>
                <input name="commercialCard_buttonText_0" type="text" placeholder="Button text">
                <input name="commercialCard_buttonLink_0" type="text" placeholder="Link">
            </div>
        </div>
        <div class="commercialCard undefined">
            <input class="font-18 red" name="commercialCard_title_1" type="text">
            <textarea class="font-16 blue" name="commercialCard_text_1" id="commercialCard_text_1"></textarea>
            <div>
                <input name="commercialCard_buttonText_1" type="text" placeholder="Button text">
                <input name="commercialCard_buttonLink_1" type="text" placeholder="Link">
            </div>
        </div>
        <div class="commercialCard undefined">
            <input class="font-18 red" name="commercialCard_title_2" type="text">
            <textarea class="font-16 blue" name="commercialCard_text_2" id="commercialCard_text_2"></textarea>
            <div>
                <input name="commercialCard_buttonText_2" type="text" placeholder="Button text">
                <input name="commercialCard_buttonLink_2" type="text" placeholder="Link">
            </div>
        </div>
        <div class="commercialCard undefined">
            <input class="font-18 red" name="commercialCard_title_3" type="text">
            <textarea class="font-16 blue" name="commercialCard_text_3" id="commercialCard_text_3"></textarea>
            <div>
                <input name="commercialCard_buttonText_3" type="text" placeholder="Button text">
                <input name="commercialCard_buttonLink_3" type="text" placeholder="Link">
            </div>
        </div>
    </div>
</div>



