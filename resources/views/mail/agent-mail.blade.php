<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<table width="500" border="0" cellspacing="0" cellpadding="0" align="center" style="border:#000 solid 1px">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#F5F5F5">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr height="60">
                                <td valign="middle" align="left" style="padding:5px;text-transform:uppercase;color:#FA741B;font-size:25px;"><b>Washington DC Auto Transport</b></td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td style="padding:10px;">
                        <table width="500" cellspacing="4" cellpadding="4" border="1">
                            <tr>
                                <td colspan="2" bgcolor="#cccccc" height="30"><b><span style="font-size:14px">Quote Information : </span></b></td>
                            </tr>
                            <tr><td width="200"><b>Ship From : </b></td><td>{{ $shipFrom }}</td></tr>
                            <tr><td width="200"><b>Ship To : </b></td><td>{{ $shipTo }}</td></tr>
                            <tr><td width="200"><b>Pickup Date : </b></td><td>{{ $time }}</td></tr>
                            <tr><td width="200"><b>Customer Name : </b></td><td>{{ $name }}</td></tr>
                            <tr><td width="200"><b>Phone Number: </b></td><td>{{ $phone }}</td></tr>
                            <tr><td width="200"><b>Email : </b></td><td>{{ $email }}</td></tr>
                            <tr><td width="200"><b>Shipping : </b></td><td>{{ $shipping }}</td></tr>
                            <tr><td width="200"><b>Operable : </b></td><td>{{ $operable }}</td></tr>
                            <tr><td colspan="2" style="padding-top:5px;" bgcolor="#F5F5F5"><b>Extra Vehicle Information :</b></td></tr>
                            @foreach($vehicle as $key => $item)
                                <tr><td colspan="2" style="padding-top:5px;" bgcolor="#F5F5F5"><b>Truck {{ $key+1 }}</b></td></tr>
                                <tr><td width="200"><b>Year : </b></td><td>{{ $item['year'] }}</td></tr>
                                <tr><td width="200"><b>Make : </b></td><td>{{ $item['make'] }}</td></tr>
                                <tr><td width="200"><b>Model : </b></td><td>{{ $item['model'] }}</td></tr>
                            @endforeach

                            <tr><td><b>IP:</b></td><td>{{ $ip }}</td></tr>
                            <tr><td colspan="2"><center><b>Sent from a Desktop browser</b></center></td></tr>
                        </table>
