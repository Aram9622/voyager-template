<table  cellpadding="5" cellspacing="5" style="margin:auto;width: 500px;height: auto;border-collapse: collapse; font-family:arial;font-size:13px" align="center" border="1">
    <tr><td style="height: 60px; background-color: #ed0509; text-align: center;width: 100%;color:white;font-size: 1.3em;"><h2>Washington DC Auto Transport</h2></td></tr>
    <tr><td style="background-color:#ccc; padding:5px;">New Contact Request:</td></tr>
</table>
<table  style="width: 500px;margin: 0 auto;padding: 8px;border-collapse: collapse;font-family:arial;font-size:13px" align="center" cellpadding="5" cellspacing="5" border="1" >
    <tr><td style="line-height: 1em;padding:5px;"> <b>Name:</b></td><td style="line-height: 1em;padding:5px;"> {{ $name }}</td></tr>
    <tr><td style="line-height: 1em;padding:5px;"> <b>Email:</b></td><td style="line-height: 1em;padding:5px;"> {{ $email }}</td></tr>
    <tr><td style="line-height: 1em;padding:5px;"> <b>Phone:</b></td><td style="line-height: 1em;padding:5px;"> {{ $phone }}</td></tr>
    <tr><td style="line-height: 1em;padding:5px;"> <b>Subject:</b></td><td style="line-height: 1em;padding:5px;"> {{ $subject }}</td></tr>
    <tr><td style="line-height: 1em;padding:5px;"> <b>Message:</b></td><td style="line-height: 1em;padding:5px;"> {{ $message }}</td></tr>
    <tr><td style="line-height: 1em;padding:5px;"> <b>IP:</b></td><td style="line-height: 1em;padding:5px;"> {{ $ip }}</td></tr>
</table>
