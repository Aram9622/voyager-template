<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
<style>
    body{background-color:#F5F5F5;font-family: "Open Sans", sans-serif;font-size:13px;}
    a{color:#E20202;text-decoration:none;}
</style>
<body>
<table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="border-radius:20px;background-color:#fff;padding:15px;  -moz-box-shadow:    3px 3px 3px 3px #E5E5E5;
  -webkit-box-shadow: 3px 3px 3px 3px #E5E5E5;
  box-shadow:         3px 3px 3px 3px #E5E5E5;">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td >
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:10px;padding-top:10px;">
                            <tr>
                                <td class="logo img" style="text-align:left;"><a href="https://newwadc.washingtondcautotransport.com/" taregt="_blank"><img src="https://austinmerchantservices.biz/example/washington/public/storage/settings/March2023/v57OxmVc1214v9aukslW.png" /> </a></td>
                                <td style=" text-align: center;" >
                                    <h2 style="color:#3B3B3C">Thanks for Requesting <br />Auto Shipping Quote</h2>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px;padding-top:10px;">
                        <table width="100%" cellspacing="4" cellpadding="4" border="0">
                            <tr heigh="60">
                                <td>
                                    <br />
                                    Dear <b>{{ $name }}</b>
                                </td>
                            </tr>
                            <tr>
                                <td  style="padding:10px;padding-top:10px;">
                                    Thank you for requesting a car shipping quote with <b>Washington DC Auto Transport!</b><br /><br />
                                    We appreciate you considering Washington DC Auto Transport for your vehicle shipping needs and we hope to soon welcome you to our long list of satisfied customers. <br /><br />

                                    Once we review your information, we will contact you with a quote for our services. We always aim to provide you with the best rates possible!<br /><br />

                                    Meanwhile, check our latest blog post <a href="https://newwadc.washingtondcautotransport.com/blogs/">here</a> and feel free to explore our posts to be on top of industry news and tendencies. And, if there is anything else we can do for you, please let us know!<br /><br />

                                </td>
                            </tr>

                            <tr>
                                <td  style="padding:10px;padding-top:10px;text-align:center;">
                                    <b>Thank you for choosing Washington DC Auto Transport!</b>
                                    <br /><br />
                                    <a href="https://www.youtube.com/watch?v=Dg5xlao93dE" target="_blank"><img src="http://www.washingtondcautotransport.com/ytube-video.jpg" width="350" height="227" /></a>
                                    <br />

                                </td>
                            </tr>
                            <tr>
                                <td style="padding:10px;padding-top:10px;">

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="width:60%;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;font-weight:bold;float:left;">
                                                    <tr><td style="padding:3px;">Phone</td><td>:</td><td><a href="tel:(443) 378-3388">(443) 378-3388</a></td></tr>

                                                    <tr><td style="padding:3px;">Email<td>:</td><td><a href="mailto:info@washingtondcautotransport.com">info@washingtondcautotransport.com</a></td></tr>

                                                    <tr><td style="padding:3px;">Website</td><td>:</td><td><a href="https://newwadc.washingtondcautotransport.com/">https://newwadc.washingtondcautotransport.com</a></td></tr>
                                                </table>
                                            </td>

                                            <td style="width:40%;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="float:right">
                                                    <tr>
                                                        <td style=" text-align: center;font-size:13px;font-weight:bold;" colspan="3">
                                                            Check our reviews<br /><br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%"><a href="https://www.yellowpages.com/washington-dc/washington-dc-auto-transport" target="_blank"><img src="http://www.washingtondcautotransport.com/images/yellowPages.png" width="35" height="35"/></a></td>

                                                        <td width="25%"><a href="https://www.yelp.com/biz/washington-dc-auto-transport-washington-2" target="_blank"><img src="http://www.washingtondcautotransport.com/images/yelpReviews.png" width="35" height="35"/></a></td>

                                                        <td width="25%"><a href="https://www.google.com/search?q=Washington+DC+Auto+Transport&ludocid=16787413938022090169&lsig=AB86z5U9-mJjiOidcx43gTQpq8K-#fpstate=lie" target="_blank"><img src="http://www.washingtondcautotransport.com/images/googleIcon.png" width="35" height="35" /></a></td>

                                                        <td width="25%"><a href="https://www.bing.com/maps?osid=14a398cd-d278-42e5-91f1-16045c9c1a29&cp=38.986884~-77.182571&lvl=12&imgid=22685956-2014-4a32-b236-839442c9ddeb&v=2&sV=2&form=S00027" target="_blank"><img src="http://www.washingtondcautotransport.com/images/bing-icon.png" width="35" height="35" /></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div style="width:100%;border-top:solid 1px #919191;font-size:12px;text-align:center;padding-top:5px;margin-top:20px;">
                                        &#169; {{ date('Y') }} Washington DC Auto Transport. All Rights Reserved.
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
