<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => "v1"], function (){
    Route::post('/login', [\App\Http\Controllers\Api\v1\Auth\AuthController::class, 'login']);
    Route::get('/posts/{slug?}', [\App\Http\Controllers\Api\v1\BlogController::class, 'getBlogs']);
    Route::get('/category', [\App\Http\Controllers\Api\v1\BlogController::class, 'getBlogsByCategory']);
    Route::post('/comments', [\App\Http\Controllers\Api\v1\BlogController::class, 'createCommentsBlog']);
    Route::get('/menus', [\App\Http\Controllers\Api\v1\TemplateResponseController::class, 'menuItems']);
    Route::get('/page/{slug}', [\App\Http\Controllers\Api\v1\TemplateResponseController::class, 'getPageBySlug']);
    Route::get('/getLast2Data',[\App\Http\Controllers\Api\v1\BlogController::class, 'getTwoLastData']);
    Route::post('/search', [\App\Http\Controllers\Api\v1\SearchController::class, 'search']);
    Route::get('/searchByTags/{tag}', [\App\Http\Controllers\Api\v1\SearchController::class, 'searchByTag']);
    Route::post('/contact', [MailController::class, 'sendEmail']);
    Route::post('/quoteForm', [\App\Http\Controllers\Api\v1\Form\QuoteFormController::class, 'sendEmailWithQuoteData']);
    Route::get('/getPagesWithTemplateType/{type}', [\App\Http\Controllers\Api\v1\TemplateResponseController::class, 'getPagesWithTemplateType']);
});

