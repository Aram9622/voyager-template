<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

     return view('welcome');
//    return redirect('admin');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::put("/posts/{id}/update", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'update'])->name("voyager.posts.update");
    Route::post("/posts/store", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'store'])->name("voyager.posts.store");
    Route::get("/posts/create", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'create'])->name("voyager.posts.create");
    Route::get("/posts/{id}/edit", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'edit'])->name("voyager.posts.edit");
    Route::get("/pages/{id}/edit", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'edit'])->name("voyager.pages.edit");
    Route::get("/posts/{id}", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'show'])->name("voyager.posts.show");
    Route::get("/posts", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'index'])->name("voyager.posts.index");
    Route::get('/pages/create',[\App\Http\Controllers\VoyagerInheritBaseController::class, 'create'])->name("voyager.pages.create");
    Route::delete("/posts/{id}", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'destroy'])->name("voyager.posts.destroy");
    Route::delete("/pages/{id}", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'destroy'])->name("voyager.pages.destroy");
    Route::post("/change-status-action/{id}", [\App\Http\Controllers\ActionStatusController::class, 'changeActionStatus']);
    Route::get("/getImageByPath", [\App\Http\Controllers\VoyagerInheritBaseController::class, 'getImageByPath'])->name('imagePath');
    Route::put('/changeMultipleRowSEO', [\App\Http\Controllers\ActionStatusController::class, 'changeSEOStatus'])->name('voyager.posts.seo.update');
    Route::get('/checkExistsSEO', [\App\Http\Controllers\ActionStatusController::class, 'checkExistsSEO']);
    Route::get('/send', [\App\Http\Controllers\SendRequestToPageSpeed::class, 'send'])->name('send');
    Route::post('/saveTemplate/{id?}', [\App\Service\TemplateService::class, 'saveTemplate'])->name('template.service');
    Route::post('/uploadTemplate', [\App\Service\TemplateService::class, 'uploadTemplate']);
    Route::post('/removeImageTemplate', [\App\Service\TemplateService::class, 'removeImage']);
    Route::put('/updateMainPage/{page}', [\App\Service\TemplateService::class, 'updateMainPage'])->name('template.updateMainPage');
    Route::get('getMainPageData/{page}', [\App\Service\TemplateService::class, 'getMainTemplateById']);
    Route::post('/uploadMediaImages', [\App\Service\TemplateService::class, 'uploadMediaImages'])->name('uploadMediaImages');
    Route::get('/refresh-from-server', [\App\Service\TemplateService::class, 'refreshFromServer'])->name('refreshFromServer');
});
