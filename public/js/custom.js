let urlMain = 'http://192.168.5.153:8000';
let urlLocal = "http://localhost:8001";
let defaultImage = "/images/image.webp";
if(location.origin !== urlMain && location.origin !== urlLocal){
    urlMain = 'https://austinmerchantservices.biz/example/washington/public';
    defaultImage = urlMain+'/'+defaultImage;
}
if(location.origin === urlLocal){
    urlMain = urlLocal
}

$(() => {
    // click on button and change status with ajax
    $('.change-status').on('click', function () {
        const btn = $(this)
        const ID = btn.data('id');
        const field = btn.data('field');
        let ACTIVE = 'Active';
        let INACTIVE = "Inactive";
        const SUCCESS = "btn-success";
        const WARNING = "btn-warning";
        let status = 0;
        let table = btn.data('table');
        if(table == 'comment'){
            ACTIVE = 'Approved';
            INACTIVE = 'Pending';
        }
        if (btn.hasClass(SUCCESS)) {
            btn.html(INACTIVE);
            btn.removeClass(SUCCESS);
            btn.addClass(WARNING);
            status = 0;
        } else {
            btn.html(ACTIVE);
            btn.removeClass(WARNING);
            btn.addClass(SUCCESS);
            status = 1;
        }

        $.post({
            url: `${urlMain}/admin/change-status-action/${ID}`,
            data: {status, field, table},
            success: function (res) {
                console.log(res);
            }
        })
    })

    function readURL(input) {
        console.log(input)
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                if ($('.panel-file').find('img').hasClass('exists-image')) {
                    $('.exists-image').attr('src', e.target.result);
                } else {
                    $('#pcimage').removeClass('d-none');
                    $('#pcimage').attr('src', e.target.result);
                }

            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.uploadImage').change(function () {
        let data = $(this).data('type')
        console.log(data)
        if (data == 'media') {
            $(this).prev().hide();
        } else {
            $('#pcimage').show()
            $(this).next().hide();
        }
        readURL($(this).find('input')[0]);
        $(this).find('span').text('Change');
        $('.clear').show()
        $('.upload-body').css('width', '250px');
        if ($('.panel-file').find('img').hasClass('exists-image')) {
            $('.exists-image').show()
        }
        $('.image-information').show();
        if (data == 'pc') {
            $('#alt').val('')
            $('#titleImage').val('')
            $('#caption').val('')
            $('#description').val('')
            $('#fileUrl').val('')
        }

    })

    $('.clear').click(function () {
        $('#media').show();
        $('#pc').show();
        $('.upload-body').css('width', '100%');
        $(this).hide()
        $('#pcimage').hide()
        $('.exists-image').hide()
        $('#pc').find('span').text('Upload');
        $('.image-information').hide();
        $('#mediaImage').val('')
        $('.exists-image').attr('src', '')
    })

    $('#media').click(function () {
        $('#exampleModal').modal('show')
    })

    $('#exampleModal').on('hidden.bs.modal', function () {
        $('#media').show()
        $('#pc').show()
        $('.upload-body').css('width', '100%');
    })

    $(document).on('click','.seo-button', function () {
        let btnSeo = $(this)
        let route = $(this).data('route')

        btnSeo.find('.loading-seo').show();
        btnSeo.find('.seoText').hide();
        btnSeo.find('i').hide();
        btnSeo.addClass('active');
        btnSeo.prop('disabled', true);
        $('.seo-button:not(.active)').prop('disabled', true)
        console.log(urlMain+'/'+route, 'seo')
        $.get({
            url: route,
            success: function (res) {
                console.log(res)
                let lighthouseResult = res.lighthouseResult;
                let score = +lighthouseResult.categories.seo.score * 100
                let statusPoint = score < 90 && score > 20 ? "warning" : score < 20 ? "danger" : score > 90 ? "success" : ""
                $('.point').text(score)
                $('.point').addClass(statusPoint);
                $('.box-success .content').html('')
                $('.box-danger .content').html('')
                $('.box-warning .content').html('')
                let countSuccess = 0;
                let countWarning = 0;
                let countDanger = 0;
                 Object.keys(lighthouseResult.audits).forEach(key => {
                     let container;
                     const item = lighthouseResult.audits[key];

                     container = item.score == 1 ? $('.box-success .content') :
                         item.score == 0 ? $('.box-danger .content') :
                             $('.box-warning .content');
                     let title = item.title.replace("<", '"').replace('>', '"');
                     if(item.score == 1){
                         countSuccess++
                     }
                     else if(item.score == 0){
                         countDanger++
                     }
                     else if(item.score == null){
                         countWarning++
                     }
                     container.append(`
                         <div class="collapse-box">
                            <div class="collapse-title d-flex">
                                <p>${title}</p>
                                <span>⮛</span>
                            </div>
                            <div class="collapse-body" style="--height:100px">
                                <p>${item.description.toString()}</p>
                            </div>
                        </div>
                     `)
                 });
                $('.box-success .count').html(countSuccess);
                $('.box-warning .count').html(countWarning);
                $('.box-danger .count').html(countDanger);
                btnSeo.find('.loading-seo').hide();
                btnSeo.find('.seoText').show();
                btnSeo.find('i').show();
                btnSeo.removeClass('active');
                btnSeo.prop('disabled', false);
                $('.seo-button:not(.active)').prop('disabled', false);
                $('#seoModal').modal('show');


            }
        })
    });

    $(document).on('click', '.collapse-box .collapse-title', function (){
        $(this).parent().toggleClass("active")
    });
    $(document).on('click','.paginate_button', function () {
        console.log('pagination')
        seoButton()
    })
    seoButton()

    $('.templateForm').on('submit', function (e){
        let title = $('#title');
        if(title.val() === ''){
            e.preventDefault();
            title.parent().find('.errorMessage').text("Title is required");
            title.parent().find('.errorMessage').addClass('text-danger');
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
    });
    $('#title').on('keyup', function () {
        $(this).parent().find('.errorMessage').removeClass('text-danger')
        $(this).parent().find('.errorMessage').text('')
    })
});
function seoButton(){
    let links = [];
    let origin = location.origin;
    let path = location.pathname
    let url = origin+getUrlTilFolder(path, 'admin');
    let imgPath = origin+getUrlTilFolder(path, 'public');

    $('.panel-collapse li').each(function (){
        links.push($(this).find('a').attr('href'));
    })
    if(window.location.href === `${url}pages`){
        links.push(`${url}pages`);
    }
    console.log(window.location.href)
    if(links.includes(window.location.href)){
        let getLastPath = window.location.href.split('/')[window.location.href.split('/').length-1]
        let model = getLastPath.charAt(0).toUpperCase() + getLastPath.slice(1).slice(0,-1);
        // https://austinmerchantservices.biz/example/back/public
        $.get({
            url: `${url}checkExistsSEO`,
            data: {model},
            success: function (res){
                $('#dataTable tbody tr').each(function (){
                    let id = $(this).find('input[name=row_id]').val();
                    res.data.forEach(post => {
                        if(post.seo && post.id == id && !$(this).find('.bread-actions').find('.seo-button').length) {
                            $(this).find('.bread-actions').append(
                                `<div>
                                    <button class="btn btn-sm btn-info pull-right seo-button " style="margin-right: 5px" data-route="${res.route}">
                                        <i class="voyager-diamond"></i>
                                        <span class="hidden-xs hidden-sm seoText">SEO</span>
                                        <span class="loading-seo" style="display: none">Analyze <img src="${imgPath}images/loading.svg" width="28px" alt=""></span>
                                    </button>
                                </div>`
                            );
                        }
                    });
                });
            }
        });

    }
}
function getUrlTilFolder(url, folderName){
    const splitedUrl = url.split('/');
    let resultUrl = '';

    for(let i = 0 ; i < splitedUrl.length ; i++){
        const str = splitedUrl[i];
        resultUrl += `${str}/`;
        if(str == folderName){
            break;
        }
    }

    return resultUrl;
}
function readURLTemplateImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(input).closest('.bgi').css({
                background: `url('${e.target.result}') no-repeat center/cover`
            })
            // if ($('.panel-file').find('img').hasClass('exists-image')) {
            //     $('.exists-image').attr('src', e.target.result);
            // } else {
            //     $('#pcimage').removeClass('d-none');
            //     $('#pcimage').attr('src', e.target.result);
            // }
        }

        reader.readAsDataURL(input.files[0]);
    }
}
$('.template-image-file').change(function (){
    $(this).closest('.bgi').addClass('showDeleteIcon');
    readURLTemplateImage($(this)[0]);
});
$('.delete-icon').on('click', function (){
    $(this).find('input').val('datark')
    $(this).parent().css('background', `url("${defaultImage}") no-repeat center/cover`);
    $(this).parent().removeClass('showDeleteIcon');
    let imageID = $(this).parent().find('input').attr('id')
    let imageName = imageID.split('_')[1];
    let parentImageKey = null;
    let pageID = $(this).closest('form').data('pageid');
    if(Number.isInteger(Number(imageName))){
        let imageArrayKey = $(this).parent().find('input').attr('name');
        parentImageKey = imageArrayKey.split('|')[1].substring(0, imageArrayKey.split('|')[1].length -2);
    }

    $.post({
        url: url+'/admin/removeImageTemplate',
        data: {imageName, parentImageKey, pageID},
        success: function (res){
            console.log(res)
        }
    })
});

$(document).ready(async function() {
    let form = $('.form-edit-add.main-page');
    let pageID = form.data('id');
    $("form.main-page *").each(function() {
        let localName = $(this)[0].localName;
        if(localName === 'textarea'){
            $(this).attr('cols', '50');
            $(this).attr('rows', '5');
        }
        let inputTags = ['input', 'textarea', 'select'];
        if(($(this).attr('type') !== 'hidden' && $(this).attr('type') !== 'checkbox') && inputTags.includes(localName)){

            $(this).addClass('form-control');
        }
    });
    if(!pageID){
        const loader = $('.scene');
        loader.css('display', 'flex')
        await getTextEditors(".page-content-template textarea");
        loader.css('display', 'none')
    }



    $.get({
        url: `${urlMain}/admin/getMainPageData/${pageID}`,
        success: async function (res) {
            if(res.data){
                const data = res.data;
                const dataKeys = Object.keys(data)
                const loader = $('.scene');
                loader.css('display', 'flex')
                await getTextEditors(".main-page textarea");
                loader.css('display', 'none')
                dataKeys.forEach( (item)=>{

                    let inputTag = $(`.main-page input[name=${item}]`);
                    let textareaTag = $(`.main-page textarea[name=${item}]`);
                    if(inputTag){
                        if(inputTag.hasClass('image-media-upload')){
                            inputTag.parent().prepend(`<p><img src="${urlMain}${data[item]}" class="image-show-media" width="250px" style="display: block" /></p>`);
                        }
                        inputTag.val(data[item])
                    }
                    if(textareaTag){
                        if(textareaTag[0]?.localName !== undefined){
                                tinymce.get(item)?.setContent(data[item] || "<p>Enter your text</p>");
                        }
                    }
                })
            }
        }
    });

});

function getTextEditors(selector){
    return  tinymce.init({
        selector: selector,
        height: 250,
        resize: false,
        toolbar: 'colors link',
        plugins: 'autoresize link',
        formats: {
            // fontname: false
        },
        upgrade: false,
        max_height: 500,
        setup: function(editor) {
            editor.on('init', function(e) {
                // console.log(e, 'text')
                // tinymce.get('transportation_text').setContent("<p>Hello World</p>")
            });
        },
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        mergetags_list: [
            { value: 'First.Name', title: 'First Name' },
            { value: 'Email', title: 'Email' },
        ],
    });
}
$('.change-bgi-position .left').on('click', function (){
    $(this).closest('.image-wrapper').find('.bgi').css('background-position', 'left');
    $('#image-position-coordinate').val('left,center')
})
$('.change-bgi-position .right').on('click', function (){
    $(this).closest('.image-wrapper').find('.bgi').css('background-position', 'right');
    $('#image-position-coordinate').val('right,center')
})
$('.change-bgi-position .top').on('click', function (){
    $(this).closest('.image-wrapper').find('.bgi').css('background-position', 'top');
    $('#image-position-coordinate').val('top,center')
})
$('.change-bgi-position .bottom').on('click', function (){
    $(this).closest('.image-wrapper').find('.bgi').css('background-position', 'bottom');
    $('#image-position-coordinate').val('bottom,center')
})
$('.image-media-upload').on('click', function (){
    let ID = $(this).attr('name');
    $('#exampleModal').modal('show');
    $('.chooseImage').attr('data-id', ID);
})

// $('#dataTable').dataTable({
//     "bDestroy": true,
//     ajax: {
//         url: ``,
//         type: "POST",
//         "data": function ( d ) {
//             return $.extend( {}, d, {
//                 "search_keywords": $("#searchALL").val(),
//                 "filter_option": $("#searchBY").val(),
//                 'permit_date' : $('#permitdate').val()
//             } );
//         }
//     },
// })

